<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Iniciar Sesión</title>

	<link rel="stylesheet" href="views/lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="views/lib/css/registroFinal.css">
	<link rel="stylesheet" href="views/lib/css/nav.css">
	<link rel="stylesheet" href="views/lib/css/sweetalert2.min.css">
	<link rel="stylesheet" href="views/lib/icons/css/all.min.css">

</head>
<body>
	<nav class="navbar fixed-top navbar-expand-lg">
		<a class="navbar-brand"><img src="views/image/logos/conversation_32.png">Bienvenido</a>
	</nav>

	<div class="padre-container">
		<div class="hijo">
			<div>
				<div>
					<h2><strong>Por último ...</strong></h2>
				</div>
			</div>
			<hr>
		
			<form id="form-usuario" <?php echo "data-idcarrera='{$_SESSION['newidcarrera']}'"; echo "data-idpersona='{$_SESSION['idnewpersona']}'";?>>
				<div class="form-row">
					<div class="form-group col-md-10">
							<input id="usuario" type="text" class="form-control" placeholder="Nombre de usuario">
					</div>

					<div class="form-group col-md-2">
						<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modalAvatar">
							<img id="img-test" src='views/image/perfiles_user/user_circle_512.png'>
						</button>
					</div>

					<!-- MODAL -->
					<div class="modal fade" id="modalAvatar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">
										Escoge tu Avatar
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
							<div id="modal-item" class="modal-body">

								<div class='row'>
									<div class='col-sm-3'>
										<a href="#" data-dismiss="modal" class='select-item' data-item='user_circle_512.png'>
											<img src='views/image/perfiles_user/user_circle_512.png'>
										</a>
									</div>
									<div class='col-sm-3'>
										<a href="#" data-dismiss="modal" class='select-item' data-item='user_pro_512.png'>
											<img src='views/image/perfiles_user/user_pro_512.png'>
										</a>
									</div>
									<div class='col-sm-3'>
										<a href="#" data-dismiss="modal" class='select-item' data-item='man_user_512.png'>
											<img src='views/image/perfiles_user/man_user_512.png'>
										</a>
									</div>
									<div class='col-sm-3'>
										<a href="#" data-dismiss="modal" class='select-item' data-item='request_512.png'>
											<img src='views/image/perfiles_user/request_512.png'>
										</a>
									</div>
								</div>

								<div class='row'>
									<div class='col-sm-3'>
										<a href="#" data-dismiss="modal" class='select-item' data-item='reply_512.png'>
											<img src='views/image/perfiles_user/reply_512.png'>
										</a>
									</div>
									<div class='col-sm-3'>
										<a href="#" data-dismiss="modal" class='select-item' data-item='user_green_512.png'>
											<img src='views/image/perfiles_user/user_green_512.png'>
										</a>
									</div>
									<div class='col-sm-3'>
										<a href="#" data-dismiss="modal" class='select-item' data-item='user_512.png'>
											<img src='views/image/perfiles_user/user_512.png'>
										</a>
									</div>
								</div>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							</div>
							</div>
						</div>
					</div>

				</div>
<br>
				<div class="form-row">
					<div class="form-group col-md-12">
						<input id="pass" type="password" class="form-control" placeholder="Contraseña">
					</div>
					<div class="form-group col-md-12">
						<input id="pass2" type="password" class="form-control" placeholder="Confirma Contraseña">
					</div>
		
				</div>

				<button type="button" id="crearCuenta" class="btn btn-primary float-right">Crear Cuenta</button>
			</form>

			<div id="test"></div>

		</div>
	</div>
</body>
<script src="views/lib/js/jquery-3.4.1.min.js"></script>
<script src="views/lib/js/sweetalert2.min.js"></script>
<script src="views/lib/js/bootstrap.min.js"></script>
<script src="views/lib/js/registroFinal.js"></script>

</html>