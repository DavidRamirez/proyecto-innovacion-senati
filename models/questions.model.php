<?php

require_once "../core/model.master.php";


class PreguntasModel extends ModelMaster
{
	public $pdo;
	
	public function __construct()
	{
		$this->pdo = parent::getConexion();
	}

	public function ListaPreguntaFamilia($idfamilia)
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_pregunta_buscar_fammilia(?)";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute(array($idfamilia));

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$pregunta = new Preguntas();

				$pregunta->__SET("idpregunta", $fila->idpregunta);
				$pregunta->__SET("idusuario", $fila->idusuario);
				$pregunta->__SET("idcarrera", $fila->idcarrera);
				$pregunta->__SET("usuario", $fila->usuario);
				$pregunta->__SET("nombrecarrera", $fila->nombrecarrera);
				$pregunta->__SET("pregunta", $fila->pregunta);
				$pregunta->__SET("textoRTF", $fila->textoRTF);
				$pregunta->__SET("dificultadfacil", $fila->dificultadfacil);
				$pregunta->__SET("dificultadmedio", $fila->dificultadmedio);
				$pregunta->__SET("dificultadhard", $fila->dificultadhard);
				$pregunta->__SET("fechahoraedicion", $fila->fechahoraedicion);
				$pregunta->__SET("fuente", $fila->fuente);
				$pregunta->__SET("estado", $fila->estado);

				$resultado[] = $pregunta;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListaPreguntaCarrera($idcarrera)
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_pregunta_buscar_carrera(?)";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute(array($idcarrera));

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$pregunta = new Preguntas();

				$pregunta->__SET("idpregunta", $fila->idpregunta);
				$pregunta->__SET("idusuario", $fila->idusuario);
				$pregunta->__SET("idcarrera", $fila->idcarrera);
				$pregunta->__SET("usuario", $fila->usuario);
				$pregunta->__SET("nombrecarrera", $fila->nombrecarrera);
				$pregunta->__SET("pregunta", $fila->pregunta);
				$pregunta->__SET("textoRTF", $fila->textoRTF);
				$pregunta->__SET("dificultadfacil", $fila->dificultadfacil);
				$pregunta->__SET("dificultadmedio", $fila->dificultadmedio);
				$pregunta->__SET("dificultadhard", $fila->dificultadhard);
				$pregunta->__SET("fechahoraedicion", $fila->fechahoraedicion);
				$pregunta->__SET("fuente", $fila->fuente);
				$pregunta->__SET("estado", $fila->estado);

				$resultado[] = $pregunta;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListaPreguntaTexto($idfamilia, $valor)
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_pregunta_buscar_texto(?,?)";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute(array($idfamilia, $valor));

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$pregunta = new Preguntas();

				$pregunta->__SET("idpregunta", $fila->idpregunta);
				$pregunta->__SET("idusuario", $fila->idusuario);
				$pregunta->__SET("idcarrera", $fila->idcarrera);
				$pregunta->__SET("usuario", $fila->usuario);
				$pregunta->__SET("nombrecarrera", $fila->nombrecarrera);
				$pregunta->__SET("pregunta", $fila->pregunta);
				$pregunta->__SET("textoRTF", $fila->textoRTF);
				$pregunta->__SET("dificultadfacil", $fila->dificultadfacil);
				$pregunta->__SET("dificultadmedio", $fila->dificultadmedio);
				$pregunta->__SET("dificultadhard", $fila->dificultadhard);
				$pregunta->__SET("fechahoraedicion", $fila->fechahoraedicion);
				$pregunta->__SET("fuente", $fila->fuente);
				$pregunta->__SET("estado", $fila->estado);

				$resultado[] = $pregunta;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function BuscarPregunta($idpregunta)
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_pregunta_buscar(?)";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute(array($idpregunta));

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$pregunta = new Preguntas();

				$pregunta->__SET("total", $fila->total);
				$pregunta->__SET("idpregunta", $fila->idpregunta);
				$pregunta->__SET("idusuario", $fila->idusuario);
				$pregunta->__SET("item_perfil", $fila->item_perfil);
				$pregunta->__SET("idcarrera", $fila->idcarrera);
				$pregunta->__SET("usuario", $fila->usuario);
				$pregunta->__SET("nombrecarrera", $fila->nombrecarrera);
				$pregunta->__SET("pregunta", $fila->pregunta);
				$pregunta->__SET("textoRTF", $fila->textoRTF);
				$pregunta->__SET("dificultadfacil", $fila->dificultadfacil);
				$pregunta->__SET("dificultadmedio", $fila->dificultadmedio);
				$pregunta->__SET("dificultadhard", $fila->dificultadhard);
				$pregunta->__SET("fechahoraedicion", $fila->fechahoraedicion);
				$pregunta->__SET("fuente", $fila->fuente);
				$pregunta->__SET("estado", $fila->estado);

				$resultado[] = $pregunta;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function BuscarPreguntaUsuario($idusuario)
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_pregunta_usuario_buscar(?)";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute(array($idusuario));

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$pregunta = new Preguntas();

				$pregunta->__SET("idpregunta", $fila->idpregunta);
				$pregunta->__SET("idusuario", $fila->idusuario);
				$pregunta->__SET("item_perfil", $fila->item_perfil);
				$pregunta->__SET("idcarrera", $fila->idcarrera);
				$pregunta->__SET("usuario", $fila->usuario);
				$pregunta->__SET("nombrecarrera", $fila->nombrecarrera);
				$pregunta->__SET("pregunta", $fila->pregunta);
				$pregunta->__SET("textoRTF", $fila->textoRTF);
				$pregunta->__SET("dificultadfacil", $fila->dificultadfacil);
				$pregunta->__SET("dificultadmedio", $fila->dificultadmedio);
				$pregunta->__SET("dificultadhard", $fila->dificultadhard);
				$pregunta->__SET("fechahoraedicion", $fila->fechahoraedicion);
				$pregunta->__SET("fuente", $fila->fuente);

				$resultado[] = $pregunta;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListaRespuestas($idpregunta)
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_respuesta_buscar(?)";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute(array($idpregunta));

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$pregunta = new Preguntas();

				$pregunta->__SET("idrespuesta", $fila->idrespuesta);
				$pregunta->__SET("idpregunta", $fila->idpregunta);
				$pregunta->__SET("idusuario", $fila->idusuario);
				$pregunta->__SET("usuario", $fila->usuario);
				$pregunta->__SET("item_perfil", $fila->item_perfil);
				$pregunta->__SET("respuesta", $fila->respuesta);
				$pregunta->__SET("fuente", $fila->fuente);
				$pregunta->__SET("fechaedicion", $fila->fechaedicion);
				$pregunta->__SET("estado", $fila->estado);

				$resultado[] = $pregunta;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ListarImagenesPregunta($idpregunta)
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_pregunta_buscar_img(?)";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute(array($idpregunta));

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$imgPregunta = new Preguntas();

				$imgPregunta->__SET("idarchivo", $fila->idarchivo);
				$imgPregunta->__SET("idpregunta", $fila->idpregunta);
				$imgPregunta->__SET("ruta", $fila->ruta);

				$resultado[] = $imgPregunta;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function CrearPregunta($idusuario, $idcarrera, $pregunta, $textoRTF, $fuente, $dificultad)
	{
		try
		{
			$query = "call spu_pregunta_registrar(?,?,?,?,?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idusuario, $idcarrera, $pregunta, $textoRTF, $fuente, $dificultad));

			$registro = $comando->fetch(PDO::FETCH_OBJ);

			return $registro;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function RespuestaPregunta($idpregunta, $idusuario, $respuesta, $fuente)
	{
		try
		{
			$query = "call spu_respuesta_pregunta(?,?,?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idpregunta, $idusuario, $respuesta, $fuente));
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function AgregarImagenPregunta($idpregunta, $ruta)
	{
		try
		{
			$query = "call spu_pregunta_insert_img(?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idpregunta, $ruta));
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function MarcarMejorRespuesta($idpregunta, $idrespuesta, $idusuariorespuesta)
	{
		try
		{
			$query = "call spu_respuesta_mejorRpta(?,?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idpregunta, $idrespuesta, $idusuariorespuesta));
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}


}



?>