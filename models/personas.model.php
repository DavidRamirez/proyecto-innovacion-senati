<?php

require_once "../core/model.master.php";


class PersonasModel extends ModelMaster
{
	public $pdo;
	
	public function __construct()
	{
		$this->pdo = parent::getConexion();
	}

	public function RegistrarPersona($apellidos, $nombres, $dni, $fechanac, $email, $telefono)
	{
		try
		{
			$query = "call spu_persona_registrar(?,?,?,?,?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($apellidos, $nombres, $dni, $fechanac, $email, $telefono));

			$registro = $comando->fetch(PDO::FETCH_OBJ);

			return $registro;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function RegistraUsuario($idpersona, $idcarrera, $idtipo, $usuario, $clave, $item_perfil)
	{
		try
		{
			$query = "call spu_usuario_registrar(?,?,?,?,?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idpersona, $idcarrera, $idtipo, $usuario, $clave, $item_perfil));

			$registro = $comando->fetch(PDO::FETCH_OBJ);

			return $registro;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function topten()
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_usuario_top10()";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute();

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$personas = new Personas();

				$personas->__SET("idusuario", $fila->idusuario);
				$personas->__SET("idpersona", $fila->idpersona);
				$personas->__SET("idcarrera", $fila->idcarrera);
				$personas->__SET("usuario", $fila->usuario);
				$personas->__SET("persona", $fila->persona);
				$personas->__SET("item_perfil", $fila->item_perfil);
				$personas->__SET("puntosAcumulados", $fila->puntosAcumulados);

				$resultado[] = $personas;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ActualizarItemPerfil($idusuario, $item_perfil)
	{
		try
		{
			$query = "call spu_usuario_actualizar_itemperfil(?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idusuario, $item_perfil));
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function ObtenerUsuario($idusuario)
	{
		try
		{
			$query = "call spu_usuario_buscar_puntos(?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idusuario));

			$registro = $comando->fetch(PDO::FETCH_OBJ);

			return $registro;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function SubirNivelUser($idusuario, $puntosusuario)
	{
		try
		{
			$query = "call spu_subir_nivel_user(?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idusuario, $item_perfil));
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function AcualizarUsuario($idpersona, $apellidos, $nombres, $dni, $fechanac, $email, $telefono)
	{
		try
		{
			$query = "call spu_persona_actualizar(?,?,?,?,?,?,?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($idpersona, $apellidos, $nombres, $dni, $fechanac, $email, $telefono));
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}
}





?>