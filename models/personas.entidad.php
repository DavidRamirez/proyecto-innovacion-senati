<?php

class Personas
{
	private $idpersona;
	private $apellidos;
	private $nombres;
	private $dni;
	private $fechanac;
	private $persona;
	private $idusuario;
	private $puntosusuario;
	private $idcarrera;
	private $usuario;
	private $item_perfil;
	private $puntosAcumulados;
	private $email;
	private $telefono;

	public function __GET($campo)
	{
		return $this->$campo;
	}

	public function __SET($campo, $valor)
	{
		$this->$campo=$valor;
	}
}
?>