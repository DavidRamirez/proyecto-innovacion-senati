<?php

require_once "../core/model.master.php";

class CarrerasModel extends ModelMaster
{
	public $pdo;
	
	public function __construct()
	{
		$this->pdo = parent::getConexion();
	}

	public function listarFamilias()
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_listar_familia()";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute();

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$familias = new Carreras();

				$familias->__SET("idfamilia", $fila->idfamilia);
				$familias->__SET("nombrefamilia", $fila->nombrefamilia);

				$resultado[] = $familias;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function listarCarreras($idfamilia)
	{
		try
		{
			$resultado = array();

			$sql = "CALL spu_listar_carreras_porFamilia(?)";
			$tabla = $this->pdo->prepare($sql);
			$tabla->execute(array($idfamilia));

			foreach ($tabla->fetchALL(PDO::FETCH_OBJ) as $fila)
			{
				$carreras = new Carreras();

				$carreras->__SET("idcarrera", $fila->idcarrera);
				$carreras->__SET("idfamilia", $fila->idfamilia);
				$carreras->__SET("nombrecarrera", $fila->nombrecarrera);

				$resultado[] = $carreras;
			}

			return $resultado;
		}
		catch (Exception $e)
		{
			die($e->getMessage());
		}
	}
}



?>