<?php
session_start();
require_once '../core/model.master.php';

class LoginModel extends ModelMaster
{
	public $pdo;

	public function __construct()
	{
		$this->pdo = parent::getConexion();
	}

	public function verificarlogeo($usuario,$clave)
	{
		try {
			$query = "CALL spu_logeo_usuario(?)";
			$comando = $this->pdo->prepare($query);
			$comando->execute(array($usuario));

			$registro = $comando->fetch(PDO::FETCH_OBJ);


			if ($registro) {
	
				if(password_verify($clave, $registro->clave)){
					$_SESSION['id'] = $registro->idusuario;
					$_SESSION['usuario'] = $registro->usuario;
					$_SESSION['idpersona'] = $registro->idpersona;
					$_SESSION['item_perfil'] = $registro->item_perfil;
					$_SESSION['idcarrera'] = $registro->idcarrera;
					$_SESSION['nombrecarrera'] = $registro->nombrecarrera;
					$_SESSION['idfamilia'] = $registro->idfamilia;
					$_SESSION['nombrefamilia'] = $registro->nombrefamilia;
					$_SESSION['idtipo'] = $registro->idtipo;
					$_SESSION['puntosAcumulados'] = $registro->puntosAcumulados;
					$_SESSION['persona'] = $registro->persona;
					$_SESSION['tipousuario'] = $registro->tipousuario;
					return true;
				}else{
					$_SESSION['id'] = "";
					$_SESSION['usuario'] = "";
					$_SESSION['idpersona'] = "";
					$_SESSION['idcarrera'] = "";
					$_SESSION['nombrecarrera'] = "";
					$_SESSION['idfamilia'] = "";
					$_SESSION['nombrefamilia'] = "";
					$_SESSION['idtipo'] = "";
					$_SESSION['puntosAcumulados'] = "";
					$_SESSION['persona'] = "";
					$_SESSION['tipousuario'] = "";
					return false;
				}

			}

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function DataUsuario(){
		try {

			$resultado = array();

			$sql = "CALL spu_usuario_buscar(?)";
			$comando = $this->pdo->prepare($sql);
			$comando->execute(array($_SESSION['id']));

			$registro = $comando->fetch(PDO::FETCH_OBJ);

			return $registro;

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


	public function CerrarSesion(){
		session_start();
		session_destroy();
		session_unset();
		header("Location: ../index.php");
		require_once "../index.php";
	}
}
	// $x = new LoginModel();
	// $x->logeo('dramirez','1234567891234567891234567891234567891234');

?>
