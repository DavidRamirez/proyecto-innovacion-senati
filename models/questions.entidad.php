<?php

class Preguntas
{
	private $idpregunta;
	private $idusuario;
	private $idcarrera;
	private $idpersona;
	private $usuario;
	private $clave;
	private $idfamilia;
	private $nombrecarrera;
	private $pregunta;
	private $respuesta;
	private $textoRTF;
	private $dificultadfacil;
	private $dificultadmedio;
	private $dificultadhard;
	private $fechahoracreacion;
	private $fechahoraedicion;
	private $fechahoraeliminacion;
	private $fuente;
	private $idtipo;
	private $estado;
	private $item_perfil;
	private $puntosAcumulados;
	private $fechacreacion;
	private $total;
	private $ruta;
	private $idarchivo;


	public function __GET($campo)
	{
		return $this->$campo;
	}

	public function __SET($campo, $valor)
	{
		$this->$campo=$valor;
	}
}
?>