<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Iniciar Sesión</title>

	<link rel="stylesheet" href="views/lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="views/lib/css/sweetalert2.min.css">
	<link rel="stylesheet" href="views/lib/css/login.css">

</head>
<body>
	<div class="padre">
		<div class="hijo">
			<div class="logo-login">
				<img src="views/image/logos/conversation_512.png" alt="">
				<hr>
			</div>
			<div class="input-login">
				<div>
					<input id="usuario" type="text" placeholder="Usuario">
				</div>

				<div>
					<input id="pass" type="password" placeholder="Contraseña">
				</div>

				<div class="btn-login">
					<button id="IniciarSession" type="button" class="btn btn-primary center-block">Iniciar Sesión</button><br> o 
					<a href="registro.php">registrate</a>
				</div>
			</div>
	</div>
</body>
<script src="views/lib/js/jquery-3.4.1.min.js"></script>
<script src="views/lib/js/sweetalert2.min.js"></script>
<script src="views/lib/js/indexprincipal.js"></script>
</html>