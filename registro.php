<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Iniciar Sesión</title>

	<link rel="stylesheet" href="views/lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="views/lib/css/registro.css">
	<link rel="stylesheet" href="views/lib/css/sweetalert2.min.css">
	<link rel="stylesheet" href="views/lib/css/nav.css">
	<link rel="stylesheet" href="views/lib/icons/css/all.min.css">

</head>
<body>
	<nav class="navbar fixed-top navbar-expand-lg">
		<a class="navbar-brand"><img src="views/image/logos/conversation_32.png">Bienvenido</a>
	</nav>

	<div class="padre">
		<div class="hijo">
			<div>
				<div>
					<h2><strong>Ingresa tus Datos </strong></h2>
				</div>
			</div>
			<hr>
		
			<form>
				<div class="form-group">
					<div class="row">
						<div class="col">
							<input id="nombre" type="text" class="form-control" placeholder="Nombre(s)" maxlength="25">
						</div>

						<div class="col">
							<input id="apellido" type="text" class="form-control" placeholder="Apellidos" maxlength="25">
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col">
							<input id="dni" type="text" class="form-control" placeholder="Dni" maxlength="7">
						</div>

						<div class="col">
							<input id="celular" type="text" class="form-control" placeholder="Celular" maxlength="9">
						</div>
					</div>
				</div>

				<div class="form-group">
					<input id="fechanac" type="text" class="form-control" id="fechaNac" placeholder="Fecha de Nacimiento">
				</div>

				<div class="form-group">
					<input id="email" type="email" class="form-control" placeholder="Correo Electronico" maxlength="50">
				</div>
	
				<a id="enviar" class="btn btn-outline-primary float-right color-white"><i class="fas fa-arrow-right"></i></a>
			</form>
			<div id="test"></div>
			
	</div>
</body>
<script type="text/javascript" src="views/lib/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="views/lib/js/sweetalert2.min.js"></script>
<script type="text/javascript" src="views/lib/js/registro.js"></script>
</html>