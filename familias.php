<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Iniciar Sesión</title>

	<link rel="stylesheet" href="views/lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="views/lib/css/familias.css">
	<link rel="stylesheet" href="views/lib/css/nav.css">
	<link rel="stylesheet" href="views/lib/icons/css/all.min.css">

</head>
<body>
	<nav class="navbar fixed-top navbar-expand-lg">
		<a href="index.php" class="navbar-brand"><img src="views/image/logos/conversation_32.png">Bienvenido</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="fas fa-bars"></span>
		</button>
	</nav>


	<div class="padre">
		<div class="hijo">
			<div>
				<button id="btn-back" type="button" class="btn btn-outline-primary float-left"><span class="fas fa-angle-left"></span></button>
				<h2 class="title"><strong>FAMILIAS OCUPACIONALES</strong></h2>
				<h4 class="sub-title"><strong>SENATI</strong></h4>
				<hr class="separador">
			</div>
			<div id="card-familias" <?php echo "data-idpersona='{$_SESSION['idnewpersona']}'"; ?>>
				<!-- FAMILIAS SE CARGARAN POR AJAX -->
			</div>
		</div>
	</div>
</body>
</html>
<script src="views/lib/js/jquery-3.4.1.min.js"></script>
<script src="views/lib/js/familias.js"></script>
