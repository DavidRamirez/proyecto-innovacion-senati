USE teamsoha_proyecto_foro;

/*************************************************************************************************************/
/*************************************************************************************************************/
															/* PREGUNTAS */
/*************************************************************************************************************/
/*************************************************************************************************************/

/*
ESTADOS DE LA PREGUNTA
0 = CERRADA
1 = ABIERTA
*/

DELIMITER $$
CREATE PROCEDURE spu_pregunta_registrar(
	IN _idusuario		SMALLINT,
    IN _idcarrera			SMALLINT,
    IN _pregunta		TEXT,
    IN _textoRTF		TEXT,
    IN _fuente			TEXT,
    IN _dificultad		SMALLINT
)
BEGIN
	DECLARE _idpregunta SMALLINT;
    DECLARE _dificultadfacil SMALLINT;
    DECLARE _dificultadmedio SMALLINT;
    DECLARE _dificultadhard SMALLINT;
    
    SET _idpregunta = (SELECT idpregunta FROM PREGUNTAS ORDER BY idpregunta DESC LIMIT 1);
    IF ISNULL(_idpregunta) THEN
		SET _idpregunta = 0;
    END IF;
    
    SET _idpregunta = _idpregunta + 1;
    SET _dificultadfacil = 0;
    SET _dificultadmedio = 0;
    SET _dificultadhard = 0;
    
    IF _dificultad  = 1 THEN
		SET _dificultadfacil = 1;
	END IF;
	IF _dificultad = 2 THEN
		SET _dificultadmedio = 1;
	END IF;
	IF _dificultad = 3 THEN
		SET _dificultadhard = 1;
	END IF;
    
    INSERT INTO PREGUNTAS(idpregunta, idusuario, idcarrera, pregunta, textoRTF, dificultadfacil, dificultadmedio, dificultadhard, fechahoracreacion, fechahoraedicion, fechahoraeliminacion, fuente, estado)
    VALUES(_idpregunta, _idusuario, _idcarrera, _pregunta, _textoRTF, _dificultadfacil, _dificultadmedio, _dificultadhard, NOW(), NOW(), NULL, _fuente, '1');

    SELECT _idpregunta;
END $$

/*
call spu_pregunta_registrar('1', '2', '¿Como reinicar ubuntu?', 'Hola que tal necesito reiniciar mi pc, esta en ubuntu', 'www.google.com.pe', '1');
call spu_pregunta_registrar('2', '2', '¿Como formatear windows?', 'Necesito descargar el iso de windwos y tambien bootearlo para poder formartear mi pc', 'www.google.com.pe', '2');
call spu_pregunta_registrar('3', '2', '¿Como detruyo una sesion en PHP?', 'Hola necesito trabajar con sesiones en PHP, pero no se por donde empezar', 'www.google.com.pe', '3');
call spu_pregunta_registrar('4', '2', '¿Como formateo ubuntu?', 'Hola que tal necesito reiniciar mi pc, esta en ubuntu', 'www.google.com.pe', '3');
call spu_pregunta_registrar('5', '2', '¿Como muevo un archivo en PHP?', 'Hola necesito mover archivos en PHP, usando sus funciones necesito ayuda', 'www.google.com.pe', '3');


*/

DELIMITER $$
CREATE PROCEDURE spu_pregunta_modificar(
	IN _idpregunta		SMALLINT,
	IN _idusuario		SMALLINT,
	IN _pregunta		TEXT,
	IN _textoRTF		TEXT,
	IN _fuente			TEXT
)
BEGIN
	UPDATE PREGUNTAS SET
		pregunta = _pregunta,
		textoRTF = _textoRTF,
		fechahoraedicion = NOW(),
		fuente	= _fuente
	WHERE idpregunta = _idpregunta AND idusuario = _idusuario;
END $$

/*
call spu_pregunta_modificar('1', '1', '¿Que boton reinicia ubuntu?', 'kljshfdalkjsaiwueyriuwoeqrhwqkjehfsaflkjsaff', 'www.google.com');

*/

DELIMITER $$
CREATE PROCEDURE spu_pregunta_eliminar(
	IN _idpregunta		SMALLINT,
	IN _idusuario		SMALLINT
)
BEGIN
	UPDATE PREGUNTAS SET
		fechahoraeliminacion = NOW(),
		estado = '0'
	WHERE idpregunta = _idpregunta AND idusuario = _idusuario;
END $$

/*
CALL spu_pregunta_eliminar('1', '1');

*/

/* ESTA LISTA IRA AL INICIO DEL FORO, NI BIEN SE LOGUE EL USER SE LE LISTARA ESAS PREGUNTAS */
DELIMITER $$
CREATE PROCEDURE spu_pregunta_buscar_fammilia(
	IN _idfamilia		SMALLINT
)
BEGIN

	SELECT idpregunta, PREGUNTAS.idusuario, PREGUNTAS.idcarrera, CONCAT(per.nombres, ' ', per.apellidos) AS usuario, nombrecarrera, pregunta, textoRTF, dificultadfacil, dificultadmedio, dificultadhard, fechahoraedicion, fuente, PREGUNTAS.estado FROM PREGUNTAS
	INNER JOIN USUARIOS usu
	ON usu.idusuario = PREGUNTAS.idusuario
	JOIN PERSONAS per
	ON per.idpersona = usu.idpersona
	INNER JOIN CARRERAS car
	ON car.idcarrera = PREGUNTAS.idcarrera
    INNER JOIN FAMILIAS fam
    ON fam.idfamilia = car.idcarrera
	WHERE car.idfamilia = _idfamilia ORDER BY fechahoracreacion DESC;

END $$

/* ESTA LISTA IRA AL INICIO DEL FORO, NI BIEN SE LOGUE EL USER SERA UN FILTRO PARA EL*/
DELIMITER $$
CREATE PROCEDURE spu_pregunta_buscar_carrera(
	IN _idcarrera		SMALLINT
)
BEGIN

	SELECT idpregunta, PREGUNTAS.idusuario, PREGUNTAS.idcarrera, CONCAT(per.nombres, ' ', per.apellidos) AS usuario, nombrecarrera, pregunta, textoRTF, dificultadfacil, dificultadmedio, dificultadhard, fechahoraedicion, fuente, PREGUNTAS.estado FROM PREGUNTAS
	INNER JOIN USUARIOS usu
	ON usu.idusuario = PREGUNTAS.idusuario
	JOIN PERSONAS per
	ON per.idpersona = usu.idpersona
	INNER JOIN CARRERAS car
	ON car.idcarrera = PREGUNTAS.idcarrera
	WHERE PREGUNTAS.idcarrera = _idcarrera ORDER BY fechahoracreacion DESC;

END $$

/* ESTA LISTA IRA AL INICIO DEL FORO, NI BIEN SE LOGUE EL USER SE LE LISTARA ESAS PREGUNTAS */
DELIMITER $$
CREATE PROCEDURE spu_pregunta_buscar_texto(
	IN _idfamilia		SMALLINT,
    IN _valor		VARCHAR(75)
)
BEGIN

	SELECT idpregunta, PREGUNTAS.idusuario, PREGUNTAS.idcarrera, CONCAT(per.nombres, ' ', per.apellidos) AS usuario, nombrecarrera, pregunta, textoRTF, dificultadfacil, dificultadmedio, dificultadhard, fechahoraedicion, fuente, PREGUNTAS.estado FROM PREGUNTAS
	INNER JOIN USUARIOS usu
	ON usu.idusuario = PREGUNTAS.idusuario
	JOIN PERSONAS per
	ON per.idpersona = usu.idpersona
	INNER JOIN CARRERAS car
	ON car.idcarrera = PREGUNTAS.idcarrera
    INNER JOIN FAMILIAS fam
    ON fam.idfamilia = car.idcarrera
	WHERE PREGUNTAS.pregunta LIKE CONCAT('%', _valor, '%') AND car.idfamilia = _idfamilia ORDER BY fechahoracreacion DESC;

END $$

/*
call spu_pregunta_buscar_texto('1', 'como');

*/

DELIMITER $$
CREATE PROCEDURE spu_pregunta_buscar(
	IN _idpregunta			SMALLINT
)
BEGIN
	DECLARE total SMALLINT;
	SET total = (SELECT COUNT(*) FROM RESPUESTAS WHERE idpregunta = _idpregunta );

	SELECT  total, idpregunta, PREGUNTAS.idusuario, PREGUNTAS.idcarrera, usu.item_perfil, CONCAT(per.nombres, ' ', per.apellidos) AS usuario, nombrecarrera, pregunta, textoRTF, dificultadfacil, dificultadmedio, dificultadhard, fechahoraedicion, fuente, PREGUNTAS.estado FROM PREGUNTAS
	INNER JOIN USUARIOS usu
	ON usu.idusuario = PREGUNTAS.idusuario
	JOIN PERSONAS per
	ON per.idpersona = usu.idpersona
	INNER JOIN CARRERAS car
	ON car.idcarrera = PREGUNTAS.idcarrera
	WHERE PREGUNTAS.idpregunta = _idpregunta;

END $$

DELIMITER $$
CREATE PROCEDURE spu_pregunta_insert_img(
	IN _idpregunta			SMALLINT,
	IN _ruta				TEXT
)
BEGIN
	DECLARE _idarchivo SMALLINT;
	SET _idarchivo = (SELECT idarchivo FROM ARCHIVOS ORDER BY idarchivo DESC LIMIT 1);
    IF ISNULL(_idarchivo) THEN
		SET _idarchivo = 0;
    END IF;
    
    SET _idarchivo = _idarchivo + 1;
    
    INSERT INTO ARCHIVOS(idarchivo, idpregunta, ruta)
    VALUES(_idarchivo, _idpregunta, _ruta);
    
END $$

DELIMITER $$
CREATE PROCEDURE spu_pregunta_usuario_buscar(
	IN _idusuario			SMALLINT
)
BEGIN

	SELECT idpregunta, PREGUNTAS.idusuario, PREGUNTAS.idcarrera, usu.item_perfil, CONCAT(per.nombres, ' ', per.apellidos) AS usuario, nombrecarrera, pregunta, textoRTF, dificultadfacil, dificultadmedio, dificultadhard, fechahoraedicion, fuente FROM PREGUNTAS
	INNER JOIN USUARIOS usu
	ON usu.idusuario = PREGUNTAS.idusuario
	JOIN PERSONAS per
	ON per.idpersona = usu.idpersona
	INNER JOIN CARRERAS car
	ON car.idcarrera = PREGUNTAS.idcarrera
	WHERE PREGUNTAS.idusuario = _idusuario ORDER BY idpregunta DESC LIMIT 10;

END $$

DELIMITER $$
CREATE PROCEDURE spu_pregunta_buscar_img(
	IN _idpregunta			SMALLINT
)
BEGIN

	SELECT idarchivo, idpregunta, ruta FROM ARCHIVOS
	WHERE idpregunta = _idpregunta;

END $$


/*************************************************************************************************************/
/*************************************************************************************************************/
																	/* RESPUESTAS */
/*************************************************************************************************************/
/*************************************************************************************************************/

DELIMITER $$
CREATE PROCEDURE spu_respuesta_pregunta(
	IN _idpregunta		SMALLINT,
	IN _idusuario		SMALLINT,
	IN _respuesta		TEXT,
	IN _fuente			TEXT
)
BEGIN
	DECLARE _idrespuesta SMALLINT;
	DECLARE _idusuariopregunta SMALLINT;
	DECLARE _puntosAcumulados SMALLINT;
	
   SET _idrespuesta = (SELECT idrespuesta FROM RESPUESTAS ORDER BY idrespuesta DESC LIMIT 1);
   SET _idusuariopregunta = (SELECT idusuario FROM PREGUNTAS WHERE idpregunta = _idpregunta);
   
   IF ISNULL(_idrespuesta) THEN
		SET _idrespuesta = 0;
   END IF;
    
   SET _idrespuesta = _idrespuesta + 1;
    
	INSERT INTO RESPUESTAS(idrespuesta, idpregunta, idusuario, respuesta, fuente, fechacreacion, fechaedicion, fechaelimado, estado)
	VALUES(_idrespuesta, _idpregunta, _idusuario, _respuesta, _fuente, NOW(), NOW(), NULL, '1');
    
   IF _idusuariopregunta <>  _idusuario THEN
		UPDATE USUARIOS SET
			puntosAcumulados = puntosAcumulados + 1
		WHERE idusuario = _idusuario;
   END IF;
   
   set _puntosAcumulados = (select puntosAcumulados from USUARIOS where idusuario = _idusuario);
   
   CALL spu_subir_nivel_user(_idusuario, _puntosAcumulados);
    
END $$

/*
call spu_respuesta_pregunta('2' ,'2', 'akjshdlsafduiewqu alsjhfaikewursadf kasjhfweufksajdfsa', 'ww.sadkjhsakjdsa');
call spu_respuesta_pregunta('2' ,'1', 'akjshdlsafduiewqu alsjhfaikewursadf kasjhfweufksajdfsa', 'ww.sadkjhsakjdsa');
call spu_respuesta_pregunta('2' ,'3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem ab quisquam, nostrum optio nam numquam? Similique ut veniam harum, velit nobis vitae optio esse praesentium ratione voluptatem, doloremque modi id!', 'www.google.com');

*/

DELIMITER $$
CREATE PROCEDURE spu_respuesta_modificar(
	IN _idrespuesta	SMALLINT,
	IN _idpregunta		SMALLINT,
	IN _idusuario		SMALLINT,
	IN _respuesta		TEXT,
	IN _fuente			TEXT
)
BEGIN
	UPDATE RESPUESTAS SET
		respuesta = _respuesta,
		fuente	 = _fuente,
		fechaedicion = NOW()
	WHERE idrespuesta = _idrespuesta AND idpregunta = _idpregunta AND idusuario = _idusuario;
END $$

/*
call spu_respuesta_modificar('1', '1', '3', 'sakjhdkasd askjda', 'asñdlañsd');

*/

DELIMITER $$
CREATE PROCEDURE spu_respuesta_eliminar(
	IN _idrespuesta	SMALLINT,
	IN _idusuario		SMALLINT
)
BEGIN
	UPDATE RESPUESTAS SET
		fechaelimado = NOW(),
		estado = '0'
	WHERE idrespuesta = _idrespuesta AND idusuario = _idusuario;
END $$

/*
call spu_respuesta_mejorRpta('3', '4', '3');

*/

DELIMITER $$
CREATE PROCEDURE spu_respuesta_mejorRpta(
	IN _idpregunta			SMALLINT,
	IN _idrespuesta			SMALLINT,
    IN _idusuariorespuesta	SMALLINT
)
BEGIN
	declare _puntosAcumulados smallint;
    set _puntosAcumulados = (select puntosAcumulados from USUARIOS where idusuario = _idusuariorespuesta);

    UPDATE PREGUNTAS SET
		estado = '0'    
	WHERE idpregunta = _idpregunta;
    
    UPDATE RESPUESTAS SET
		estado = '0'    
	WHERE idrespuesta = _idrespuesta;
    
    UPDATE USUARIOS SET
		puntosAcumulados = puntosAcumulados + 4
	WHERE idusuario = _idusuariorespuesta;
    
   CALL spu_subir_nivel_user(_idusuariorespuesta, _puntosAcumulados);

END $$

DELIMITER $$
CREATE PROCEDURE spu_respuesta_buscar(
	IN _idpregunta		SMALLINT
)
BEGIN

	SELECT idrespuesta, idpregunta, RESPUESTAS.idusuario, CONCAT(per.nombres, ' ', per.apellidos) AS usuario, item_perfil, respuesta, fuente, fechaedicion, RESPUESTAS.estado FROM RESPUESTAS
	INNER JOIN USUARIOS usu
	ON usu.idusuario = RESPUESTAS.idusuario
	JOIN PERSONAS per
	ON per.idpersona = usu.idpersona
	WHERE idpregunta = _idpregunta ORDER BY fechaedicion DESC;

END $$




/*************************************************************************************************************/
/*************************************************************************************************************/
																	/* LOGIN*/
/*************************************************************************************************************/
/*************************************************************************************************************/

DELIMITER $$
CREATE PROCEDURE spu_logeo_usuario(
	IN _usuario			VARCHAR(25)
)
BEGIN

	SELECT idusuario, USUARIOS.idpersona, USUARIOS.idcarrera, CARRERAS.nombrecarrera, CARRERAS.idfamilia, FAMILIAS.nombrefamilia, USUARIOS.idtipo, usuario, clave, item_perfil, fechacreacion, puntosAcumulados, CONCAT(apellidos, ', ', nombres) AS persona, tipousuario FROM USUARIOS
	INNER JOIN PERSONAS
    ON PERSONAS.idpersona = USUARIOS.idpersona
    INNER JOIN CARRERAS
    ON CARRERAS.idcarrera = USUARIOS.idcarrera
    JOIN FAMILIAS
    ON FAMILIAS.idfamilia = CARRERAS.idfamilia
    INNER JOIN TIPOUSUARIO
    ON TIPOUSUARIO.idtipo  = USUARIOS.idtipo
    WHERE usuario = _usuario AND estado = '1';

END $$

/*
call spu_logeo_usuario('dramirez');

*/

/*************************************************************************************************************/
/*************************************************************************************************************/
																	/* PERSONA - USUARIO*/
/*************************************************************************************************************/
/*************************************************************************************************************/
DELIMITER $$
CREATE PROCEDURE spu_persona_registrar(
	IN _apellidos			VARCHAR(25),
	IN _nombres				VARCHAR(25),
    IN _dni					CHAR(8),
    IN _fechanac			DATE,
    IN _email				VARCHAR(50),
    IN _telefono			CHAR(9)
)
BEGIN
	DECLARE _idpersona SMALLINT;
    
    SET _idpersona = (SELECT idpersona FROM PERSONAS ORDER BY idpersona DESC LIMIT 1);
    
    IF ISNULL(_idpersona) THEN
		SET _idpersona = 0;
    END IF;
    
    SET _idpersona = _idpersona + 1;
    
	INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono)
	VALUES(_idpersona, _apellidos, _nombres, _dni, _fechanac, _email, _telefono);
    
    SELECT idpersona FROM PERSONAS ORDER BY idpersona DESC LIMIT 1;
END $$

DELIMITER $$
CREATE PROCEDURE spu_persona_actualizar(
	IN _idpersona			smallint,
	IN _apellidos			VARCHAR(25),
	IN _nombres				VARCHAR(25),
    IN _dni					CHAR(8),
    IN _fechanac			DATE,
    IN _email				VARCHAR(50),
    IN _telefono			CHAR(9)
)
BEGIN
    
    update PERSONAS set
		apellidos = _apellidos,
		nombres = _nombres,
        dni = _dni,
        fechanac = _fechanac,
        email = _email,
        telefono = _telefono
	where idpersona = _idpersona;

END $$

DELIMITER $$
CREATE PROCEDURE spu_usuario_registrar(
	IN _idpersona		SMALLINT,
    IN _idcarrera		SMALLINT,
    IN _idtipo			SMALLINT,
    IN _usuario			VARCHAR(25),
    IN _clave			CHAR(60),
    IN _item_perfil	VARCHAR(20)
)
BEGIN
	DECLARE _idusuario SMALLINT;
    DECLARE _idprogreso SMALLINT;
    
    SET _idusuario = (SELECT idusuario FROM USUARIOS ORDER BY idusuario DESC LIMIT 1);
    SET _idprogreso = (SELECT idprogreso FROM PROGRESO ORDER BY idprogreso DESC LIMIT 1);
    
    IF ISNULL(_idusuario) THEN
		SET _idusuario = 0;
    END IF;
    IF ISNULL(_idprogreso) THEN
		SET _idprogreso = 0;
    END IF;
    
    SET _idusuario = _idusuario + 1;
    SET _idprogreso = _idprogreso + 1;
    
	INSERT INTO USUARIOS(idusuario, idpersona, idcarrera, idtipo, usuario, clave, item_perfil, fechacreacion, puntosAcumulados, estado)
	VALUES(_idusuario, _idpersona, _idcarrera, _idtipo, _usuario, _clave, _item_perfil, NOW(), 0, 1);
    
    INSERT INTO PROGRESO(idprogreso, idusuario, idnivel, fechahora)
    VALUES(_idprogreso, _idusuario, '1', NOW());
        
    CALL spu_logeo_usuario(_usuario);
END $$

/*
call spu_usuario_registrar('4', '7', '3', 'snombsre', '$2y$10$f/33PScapk1SU0GVg100eOzyuydz1hLAt.KOnI5cIiIC1vvARiHdG', 'user_512');

*/

DELIMITER $$
CREATE PROCEDURE spu_usuario_top10()
BEGIN

	SELECT idusuario, USUARIOS.idpersona, USUARIOS.idcarrera, usuario, CONCAT(nombres,' ',apellidos)AS persona, item_perfil, puntosAcumulados FROM USUARIOS
    INNER JOIN PERSONAS
    ON PERSONAS.idpersona = USUARIOS.idpersona
    INNER JOIN CARRERAS
    ON CARRERAS.idcarrera = USUARIOS.idcarrera
    ORDER BY puntosAcumulados DESC LIMIT 10;

END $$

DELIMITER $$
CREATE PROCEDURE spu_usuario_actualizar_itemperfil(
	IN _idusuario			SMALLINT,
	IN _itemperfil			VARCHAR(20)
)
BEGIN

	UPDATE USUARIOS SET
		item_perfil = _itemperfil
	WHERE idusuario = _idusuario;

END $$

DELIMITER $$
CREATE PROCEDURE spu_usuario_buscar(
	IN _idusuario		SMALLINT
)
BEGIN

	SELECT idprogreso, PROGRESO.idnivel, NIVELES.nombre, avatar, USUARIOS.idusuario, USUARIOS.idpersona, USUARIOS.idcarrera, CARRERAS.nombrecarrera, CARRERAS.idfamilia, FAMILIAS.nombrefamilia, USUARIOS.idtipo, usuario, clave, item_perfil, fechacreacion, puntosAcumulados, CONCAT(apellidos, ', ', nombres) AS persona, PERSONAS.apellidos, PERSONAS.nombres, PERSONAS.dni, email, PERSONAS.fechanac, PERSONAS.telefono, tipousuario FROM PROGRESO
    INNER JOIN NIVELES
    ON NIVELES.idnivel = PROGRESO.idnivel
    INNER JOIN USUARIOS
    ON USUARIOS.idusuario = PROGRESO.idusuario
	JOIN PERSONAS
    ON PERSONAS.idpersona = USUARIOS.idpersona
    INNER JOIN CARRERAS
    ON CARRERAS.idcarrera = USUARIOS.idcarrera
    JOIN FAMILIAS
    ON FAMILIAS.idfamilia = CARRERAS.idfamilia
    JOIN TIPOUSUARIO
    ON TIPOUSUARIO.idtipo  = USUARIOS.idtipo
    WHERE PROGRESO.idusuario = _idusuario AND USUARIOS.estado = '1';

END $$

DELIMITER $$
CREATE PROCEDURE spu_usuario_buscar_puntos(
	IN _idusuario		SMALLINT
)
BEGIN

	SELECT * FROM USUARIOS
   WHERE idusuario = _idusuario;

END $$

/*************************************************************************************************************/
/*************************************************************************************************************/
																	/* FAMILIAS - CARRERAS*/
/*************************************************************************************************************/
/*************************************************************************************************************/
DELIMITER $$
CREATE PROCEDURE spu_listar_familia()
BEGIN
	SELECT * FROM FAMILIAS ORDER BY idfamilia ASC;
END $$

DELIMITER $$
CREATE PROCEDURE spu_listar_carreras_porFamilia(
	IN _idfamilia			SMALLINT
)
BEGIN
	SELECT idcarrera, CARRERAS.idfamilia, nombrefamilia, nombrecarrera FROM CARRERAS
    INNER JOIN FAMILIAS
    ON FAMILIAS.idfamilia = CARRERAS.idfamilia
    WHERE CARRERAS.idfamilia = _idfamilia;
END $$

/*
call spu_listar_carreras_porFamilia('2');

call spu_listar_familia();

*/


/*************************************************************************************************************/
/*************************************************************************************************************/
																	/* PROGRESO - NIVELES*/
/*************************************************************************************************************/
/*************************************************************************************************************/

DELIMITER $$
CREATE PROCEDURE spu_subir_nivel_user(
	IN _idusuario			SMALLINT,
	IN _puntosusuario		SMALLINT
)
BEGIN
	DECLARE _idnivel SMALLINT;
	DECLARE _puntosnivel SMALLINT;
	DECLARE _idniveluser SMALLINT;

	SET _idnivel = (SELECT idnivel FROM NIVELES WHERE puntosnivel <= _puntosusuario ORDER BY puntosnivel DESC LIMIT 1);
	SET _puntosnivel = (SELECT puntosnivel FROM NIVELES WHERE puntosnivel <= _puntosusuario ORDER BY puntosnivel DESC LIMIT 1);
	SET _idniveluser = (SELECT idnivel FROM PROGRESO WHERE idusuario = _idusuario);
	
	IF _idnivel > _idniveluser THEN
		IF _puntosusuario >= _puntosnivel THEN
			UPDATE PROGRESO SET
				idnivel = idnivel + 1
			WHERE idusuario = _idusuario;
		END IF;
	END IF;
    
    select _idnivel, _puntosnivel, _puntosusuario, _idniveluser;

END $$

