DROP DATABASE IF EXISTS teamsoha_proyecto_foro;

CREATE DATABASE teamsoha_proyecto_foro;

USE teamsoha_proyecto_foro;

CREATE TABLE TIPOUSUARIO(
	idtipo			SMALLINT NOT NULL,
    tipousuario		VARCHAR(21) NOT NULL,
    CONSTRAINT pk_idtipo_tipo PRIMARY KEY(idtipo),
    CONSTRAINT uk_tipousuario_tipo UNIQUE(tipousuario)
)ENGINE=INNODB;

/*
INSERT INTO TIPOUSUARIO(idtipo, tipousuario) VALUES('1', 'Administrador');
INSERT INTO TIPOUSUARIO(idtipo, tipousuario) VALUES('2', 'Instructor');
INSERT INTO TIPOUSUARIO(idtipo, tipousuario) VALUES('3', 'Usuario');

*/

CREATE TABLE PERSONAS(
	idpersona		SMALLINT NOT NULL,
	apellidos		VARCHAR(25) NOT NULL,
    nombres			VARCHAR(25) NOT NULL,
    dni				CHAR(8) NULL,
    fechanac		DATE NULL,
    email			VARCHAR(50) NULL,
    telefono		CHAR(9) NULL,
    CONSTRAINT pk_idpersona_persona PRIMARY KEY(idpersona),
    CONSTRAINT uk_persona_persona UNIQUE(apellidos, nombres),
    CONSTRAINT uk_dni_persona UNIQUE(dni)    
)ENGINE=INNODB;

/*
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('1', 'Ramirez Villegas', 'David', '75570735', '2000-04-24', 'ramirezvillegasdavid0@gmail.com', '934212830');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('2', 'Amoretti Almeyda', 'Julio', '75570736', '2000-04-25', 'jamoretti@gmail.com', '934212831');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('3', 'Lopez Loayza', 'Alvaro', '75570737', '2000-04-26', 'alopez@gmail.com', '934212832');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('4', 'Sessarego Roman', 'Angelo', '75570731', '2000-04-26', 'alopez2@gmail.com', '934212833');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('5', 'Zaquarias napan', 'Omar', '75570732', '2000-04-26', 'alopez3@gmail.com', '934212834');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('6', 'Canto Fernandez', 'Daniel', '75570733', '2000-04-26', 'alopez4@gmail.com', '934212835');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('7', 'Flores Lume', 'Alexis', '75570734', '2000-04-26', 'alopez5@gmail.com', '934212836');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('8', 'Yataco Castilla', 'Daniel', '75570738', '2000-04-26', 'alopez@gmail.com', '934212837');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('9', 'Cruz Mauricio', 'Niver', '75570739', '2000-04-26', 'alopez@gmail.com', '934212838');
INSERT INTO PERSONAS(idpersona, apellidos, nombres, dni, fechanac, email, telefono) VALUES('10', 'Tenorio Cartagena', 'Jose', '75570730', '2000-04-26', 'alopez@gmail.com', '934212839');

*/

CREATE TABLE FAMILIAS(
	idfamilia		SMALLINT NOT NULL,
    nombrefamilia	VARCHAR(45) NOT NULL,
    CONSTRAINT pk_idfamilia_familia PRIMARY KEY(idfamilia),
    CONSTRAINT uk_nomfmailia_familia UNIQUE(nombrefamilia)
)ENGINE=INNODB;

/*
insert into FAMILIAS(idfamilia, nombrefamilia) values('1', 'TECNOLOGÍAS DE LA INFORMACIÓN');
insert into FAMILIAS(idfamilia, nombrefamilia) values('2', 'METALMECÁNICA');
insert into FAMILIAS(idfamilia, nombrefamilia) values('3', 'ELECTROTECNIA');
insert into FAMILIAS(idfamilia, nombrefamilia) values('4', 'ADMINISTRACIÓN DE EMPRESAS');
insert into FAMILIAS(idfamilia, nombrefamilia) values('5', 'CONFECCIONES');

*/

CREATE TABLE CARRERAS(
	idcarrera		SMALLINT NOT NULL,
    idfamilia		SMALLINT NOT NULL,
    nombrecarrera	VARCHAR(100) NOT NULL,
    CONSTRAINT pk_idcarrera_carrera PRIMARY KEY(idcarrera),
    CONSTRAINT fk_idfamilia_carrera FOREIGN KEY(idfamilia) REFERENCES FAMILIAS(idfamilia),
    CONSTRAINT uk_nombrecarrera_carrera UNIQUE(nombrecarrera)
)ENGINE=INNODB;

/*
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('1', '1', 'Diseño Gráfico Digital');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('2', '1', 'Ingenieria de Software con Inteligencia Artificial');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('3', '2', 'Mecánico de Mantenimiento');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('4', '2', 'Soldador Universal');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('5', '3', 'Electricista Industrial');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('6', '4', 'Administración Industrial');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('7', '4', 'Seguridad Industrial y Prevención de Riesgos');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('8', '4', 'Administración Logística');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('9', '5', 'Tecnología de Procesos de Producción de Prendas de Vestir');
insert into CARRERAS(idcarrera, idfamilia, nombrecarrera) VALUES('10', '5', 'Tecnología del Diseño y Desarrollo de Prendas de Vestir');

*/

CREATE TABLE USUARIOS(
	 idusuario		SMALLINT NOT NULL,
    idpersona		SMALLINT NOT NULL,
    idcarrera		SMALLINT NOT NULL,
    idtipo			SMALLINT NOT NULL,
    usuario			VARCHAR(25) NOT NULL,
    clave			CHAR(60) NOT NULL,
    item_perfil	VARCHAR(20) NULL,
    fechacreacion	DATETIME NOT NULL,
    puntosAcumulados SMALLINT NOT NULL,
    estado			CHAR(1) NOT NULL,
    CONSTRAINT pk_idusuario_usuario PRIMARY KEY(idusuario),
    CONSTRAINT fk_idpersona_usuario FOREIGN KEY(idpersona) REFERENCES PERSONAS(idpersona),
    CONSTRAINT fk_idcarrera_usuario FOREIGN KEY(idcarrera) REFERENCES CARRERAS(idcarrera),
    CONSTRAINT fk_idtipo_usuario FOREIGN KEY(idtipo) REFERENCES TIPOUSUARIO(idtipo),
    CONSTRAINT fk_usuario_usuario UNIQUE(usuario)
)ENGINE=INNODB;

/* PASS: 123456 */
/*
INSERT INTO USUARIOS VALUES('1', '1', '1', '1', 'dramirez', '$2y$10$f/33PScapk1SU0GVg100eOzyuydz1hLAt.KOnI5cIiIC1vvARiHdG', 'request_512.png', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('2', '2', '1', '2', 'jamoretti', '$2y$10$VdtqJrxXYh4o98F.0ng/humW3q89kMaio0JnEowAoWFgczTqtT8IO', 'user_circle_512.png', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('3', '3', '1', '1', 'alopez', '$2y$10$N2bc7OrAbD8C9jVEOedsK.wkPvSTeMDUSytOLVaWqtmRWzIJfYmie', 'user_pro_512.png', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('4', '4', '1', '2', 'asessarego', '$2y$10$N2bc7OrAbD8C9jVEOedsK.wkPvSTeMDUSytOLVaWqtmRWzIJfYmie', 'uses.jpg', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('5', '5', '1', '1', 'ozaquearias', '$2y$10$N2bc7OrAbD8C9jVEOedsK.wkPvSTeMDUSytOLVaWqtmRWzIJfYmie', 'reply_512.png', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('6', '6', '1', '2', 'dcanto', '$2y$10$N2bc7OrAbD8C9jVEOedsK.wkPvSTeMDUSytOLVaWqtmRWzIJfYmie', 'user_512.png', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('7', '7', '1', '1', 'aflores', '$2y$10$N2bc7OrAbD8C9jVEOedsK.wkPvSTeMDUSytOLVaWqtmRWzIJfYmie', 'user_green_512.png', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('8', '8', '1', '2', 'dyataco', '$2y$10$N2bc7OrAbD8C9jVEOedsK.wkPvSTeMDUSytOLVaWqtmRWzIJfYmie', 'man_user_512.png', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('9', '9', '1', '1', 'ncruz', '$2y$10$N2bc7OrAbD8C9jVEOedsK.wkPvSTeMDUSytOLVaWqtmRWzIJfYmie', 'user_pro_512.png', NOW(), '0', '1');
INSERT INTO USUARIOS VALUES('10', '10', '1', '2', 'jtenorio', '$2y$10$N2bc7OrAbD8C9jVEOedsK.wkPvSTeMDUSytOLVaWqtmRWzIJfYmie', 'user_circle_512.png', NOW(), '0', '1');

*/

CREATE TABLE HABILIDADES(
	idhabilidad		SMALLINT NOT NULL,
    idusuario		SMALLINT NOT NULL,
    nombrehabilidad VARCHAR(30) NULL,
    escalavalorativa VARCHAR(30) NULL,
    experiencia		TINYINT NULL,
    CONSTRAINT pk_idhabilidad_habilidades PRIMARY KEY(idhabilidad),
    CONSTRAINT fk_idusuario_habilidades FOREIGN KEY(idusuario) REFERENCES USUARIOS(idusuario),
    CONSTRAINT uk_habilida_habilidades UNIQUE(idhabilidad, nombrehabilidad)
)ENGINE=INNODB;


CREATE TABLE NIVELES(
	idnivel			SMALLINT NOT NULL,
    nombre			VARCHAR(20) NOT NULL,
    avatar			VARCHAR(50) NOT NULL,
    puntosnivel		SMALLINT NOT NULL,
    CONSTRAINT pk_idnivel_nivel PRIMARY KEY(idnivel),
    CONSTRAINT uk_nombre_nivel UNIQUE(nombre)
)ENGINE=INNODB;

/*
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('1', 'Pollito', 'pollito', '0');
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('2', 'Cachimbo', 'cachimbo', '50');
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('3', 'Basico', 'basico', '125');
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('4', 'Practicante', 'practicante', '200');
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('5', 'Graduado', 'graduado', '300');
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('6', 'Medalla de Bronce', 'm_bronce', '400');
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('7', 'Medalla de Plata', 'm_plata', '550');
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('8', 'Medalla de Oro', 'm_oro', '800');
insert into NIVELES(idnivel, nombre, avatar, puntosnivel) values('9', 'Medalla de Diamante', 'm_diamante', '1000');

*/

CREATE TABLE PROGRESO(
	idprogreso		SMALLINT NOT NULL,
    idusuario		SMALLINT NOT NULL,
    idnivel			SMALLINT NOT NULL,
    fechahora		DATETIME NOT NULL,
    CONSTRAINT pk_idprogreso_progreso PRIMARY KEY(idprogreso),
    CONSTRAINT fk_usuario_usuario FOREIGN KEY(idusuario) REFERENCES USUARIOS(idusuario),
    CONSTRAINT uk_usuario_usuario UNIQUE(idusuario),
    CONSTRAINT fk_idnivel_usuario FOREIGN KEY(idnivel) REFERENCES NIVELES(idnivel)
)ENGINE=INNODB;

/*
insert into PROGRESO(idprogreso, idusuario, idnivel, fechahora) values('1', '1', '1', now());
insert into PROGRESO(idprogreso, idusuario, idnivel, fechahora) values('2', '2', '1', now());
insert into PROGRESO(idprogreso, idusuario, idnivel, fechahora) values('3', '3', '1', now());

*/

CREATE TABLE AMONESTACIONES(
	idamonestacion	SMALLINT NOT NULL,
    nombre			VARCHAR(20) NOT NULL,
    descripcion		VARCHAR(50) NULL,
    puntajecontra	SMALLINT NOT NULL,
    CONSTRAINT pk_idamonestacion_amones PRIMARY KEY(idamonestacion),
    CONSTRAINT uk_nombre_amones UNIQUE(nombre)
)ENGINE=INNODB;

CREATE TABLE BAN(
		idban			SMALLINT NOT NULL,
    idusuario		SMALLINT NOT NULL,
    idamonestacion	SMALLINT NOT NULL,
    motivo			VARCHAR(30) NULL,
    fechahoraban	DATETIME NOT NULL,
    CONSTRAINT pk_idban_ban PRIMARY KEY(idban),
    CONSTRAINT fk_idusuario_ban FOREIGN KEY(idusuario) REFERENCES USUARIOS(idusuario),
    CONSTRAINT fk_idamonestacion_ban FOREIGN KEY(idamonestacion) REFERENCES AMONESTACIONES(idamonestacion)
)ENGINE=INNODB;


CREATE TABLE PREGUNTAS(
	idpregunta		SMALLINT NOT NULL,
    idusuario		SMALLINT NOT NULL,
    idcarrera		SMALLINT NOT NULL,
    pregunta		TEXT NOT NULL,
    textoRTF		TEXT NOT NULL,
    dificultadfacil	TINYINT NOT NULL,
    dificultadmedio	TINYINT NOT NULL,
    dificultadhard	TINYINT NOT NULL,
    fechahoracreacion DATETIME NOT NULL,
    fechahoraedicion  DATETIME NULL,
    fechahoraeliminacion DATETIME NULL,
    fuente			TEXT NULL,
    estado			CHAR(1),
    CONSTRAINT pk_idpregunta_preguntas PRIMARY KEY(idpregunta),
    CONSTRAINT fk_idusuario_preguntas FOREIGN KEY(idusuario) REFERENCES USUARIOS(idusuario),
    CONSTRAINT fk_idcarrera_preguntas FOREIGN KEY(idcarrera) REFERENCES CARRERAS(idcarrera)
)ENGINE=INNODB;

/*

select * from preguntas

*/

CREATE TABLE ARCHIVOS(
	idarchivo		SMALLINT NOT NULL,
    idpregunta		SMALLINT NOT NULL,
    ruta			VARCHAR(200) NOT NULL,
    CONSTRAINT pk_idarchivo_archivo PRIMARY KEY(idarchivo),
    CONSTRAINT fk_idpregunta_archivo FOREIGN KEY(idpregunta) REFERENCES PREGUNTAS(idpregunta)
)ENGINE=INNODB;

CREATE TABLE RESPUESTAS(
	idrespuesta		SMALLINT NOT NULL,
    idpregunta		SMALLINT NOT NULL,
    idusuario		SMALLINT NOT NULL,
    respuesta		TEXT NOT NULL,
    fuente			TEXT NULL,
    fechacreacion	DATETIME NOT NULL,
    fechaedicion	DATETIME NULL,
    fechaelimado	DATETIME NULL,
    estado			CHAR(1),
    CONSTRAINT pk_idrespuesta_respuestas PRIMARY KEY(idrespuesta),
    CONSTRAINT fk_idpregunta_respuestas FOREIGN KEY(idpregunta) REFERENCES PREGUNTAS(idpregunta),
    CONSTRAINT fk_idusuario_respuestas FOREIGN KEY(idusuario) REFERENCES USUARIOS(idusuario)
)ENGINE=INNODB;

CREATE TABLE NOTIFICACIONES(
	idnotificacion	SMALLINT NOT NULL,
    tiponotificacion CHAR(1) NOT NULL,
    mensajeRTF		TEXT NOT NULL,
    idemisor		SMALLINT NOT NULL,
    idreceptor		SMALLINT NOT NULL,
    fechahoramoderacion DATETIME NOT NULL,
    adjunto			VARCHAR(40) NULL,
    CONSTRAINT pk_idnotificacion_notificacion PRIMARY KEY(idnotificacion),
    CONSTRAINT fk_idemisor_notificacion FOREIGN KEY(idemisor) REFERENCES USUARIOS(idusuario),
    CONSTRAINT fk_idreceptor_notificacion FOREIGN KEY(idreceptor) REFERENCES USUARIOS(idusuario)
)ENGINE=INNODB;
