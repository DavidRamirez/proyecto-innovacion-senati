<?php
session_start();
require_once "../models/questions.model.php";
require_once "../models/questions.entidad.php";

$objPreguntasModel = new PreguntasModel();

if (isset($_GET['operacion']))
{
	if ($_GET['operacion'] == 'listarPregunta') {

		$tabla = $objPreguntasModel->BuscarPregunta($_GET['idpregunta']);
		$tablaImg = $objPreguntasModel->ListarImagenesPregunta($_GET['idpregunta']);

		foreach ($tabla as $fila) {
			$date = date_create($fila->fechahoraedicion);
			$fechaformato = date_format($date, 'd-m-Y H:i:s');
			$fecha = substr($fechaformato, 0, 10);
			$dificultad = "";
			$myquestion = "Pregunta creada el ";
			$item_perfil = $_SESSION['item_perfil'];
			$closequestion = '';

			$formRptaUsuario = "<hr>
		<div class='reply-user-now row'>
			<div class='reply-user col-sm-2 col-md-2 col-lg-1 text-center'>
				<img class='img-user-reply' src='image/perfiles_user/{$item_perfil}'>
			</div>
			<div class='reply-input col-sm-10 col-md-10 col-lg-11'>
				<div class='form-group'>
					<div class='row'>
						<div class='col-10 col-sm-10 col-md-10 col-lg-11'>
							<textarea class='form-control' type='text' id='user-reply'></textarea>
							<div class='collapse' id='collapseExample'>
								<input class='form-control' type='text' id='link-reply' placeholder='Link: www.google.com.pe'>
							</div>
						</div>
						<div class='col-2 col-sm-2 col-md-2 col-lg-1'>
							<button class='btn btn-outline-primary' type='button' data-toggle='collapse' data-toggle='tooltip' data-placement='bottom' title='Añadir enlace' data-target='#collapseExample' aria-expanded='false' aria-controls='collapseExample'><span class='fas fa-link'></span></button>
						</div>
					</div>
				</div>
				
				<button class='btn btn-primary' id='enviar'>Enviar</button>
			</div>
		</div>";

			if ($fila->estado == '0') {
				$closequestion = "<span id='close-question' class='fas fa-check-square'></span> [Resuelto] - ";
				$formRptaUsuario = "<hr> La pregunta tuvo una respuesta satisfactoria";
			}


			if ($fila->total == 1) {
				$total = $fila->total . " respuesta";
			}else{
				$total = $fila->total . " respuestas";
			}

			if ($fila->dificultadfacil == '1') {
				$dificultad = "flag-green";
			}
			if ($fila->dificultadmedio == '1') {
				$dificultad = "flag-orange";
			}
			if ($fila->dificultadhard == '1') {
				$dificultad = "flag-red";
			}

			if ($fila->idusuario == $_SESSION['id']) {
				$myquestion = "Creaste esta pregunta el ";
			}else{
				$myquestion = "Pregunta creada por <strong>{$fila->usuario}</strong> el ";
			}

			echo "<div class='row'>
					<div class='question-user col-sm-2 col-md-2 col-lg-1 text-center' data-estadopregunta='{$fila->estado}'>
						<img class='profile-user' src='image/perfiles_user/{$fila->item_perfil}'>
					</div>
					<div class='question-contend col-sm-10 col-md-10 col-lg-11'>
						<span class='question-date'>{$myquestion}<strong>{$fecha}</strong></span>	
						<h4>{$closequestion} {$fila->pregunta}</h4>
						<span>{$fila->textoRTF}</span>
						<p>
							<a href='http://{$fila->fuente}' target='_blank'>
								<span class='fas fa-external-link-alt text-primary'></span>
								{$fila->fuente}
							</a>
						</p>
					<br>
						<div class='question-data'>
							<span class='fas fa-flag {$dificultad}'></span>
							<h6>{$total}</h6>
						</div>
					</div>
				  </div>
				<div class='row contend-img'>";
		}
		foreach ($tablaImg as $filaImg) {
			if ($filaImg->ruta != "") {
				echo "
					<div class=' col-sm-2 col-md-3 col-lg-3'>
						<a data-fancybox='gallery' href='{$filaImg->ruta}'>
							<img class='img-question' src='{$filaImg->ruta}'>
						</a>
					</div>
				";
			}
		}

		echo "</div>{$formRptaUsuario}";

	}



	if ($_GET['operacion'] == 'listarPreguntaUsuario') {

		$tabla = $objPreguntasModel->BuscarPreguntaUsuario($_GET['idusuario']);

		foreach ($tabla as $fila) {
			echo "
				<div class='question'>
					<h5><strong>{$fila->pregunta}</strong></h5>
					<span>{$fila->textoRTF}</span>
				</div>
			";
		}

	}

	if ($_GET['operacion'] == 'listarRespuestas') {

		$tabla = $objPreguntasModel->ListaRespuestas($_GET['idpregunta']);

		foreach ($tabla as $fila) {
			$date = date_create($fila->fechaedicion);
			$fechaformato = date_format($date, 'd-m-Y H:i:s');
			$fecha = substr($fechaformato, 0, 10);
			$classstar = "";
			$classreply = "";

			if ($fila->estado == '0') {
				$classstar = "fas fa-star best-reply";
				$classreply = "reply row reply-hover";
			}else{
				$classstar = "far fa-star best-reply";
				$classreply = "reply row";
			}
			
			echo "<div class='{$classreply}'><div class='reply-user col-sm-2 col-md-2 col-lg-1 text-center'>
				<img class='profile-user' src='image/perfiles_user/{$fila->item_perfil}'>
				</div>
				<div class='reply-contend col-sm-10 col-md-10 col-lg-11' data-estado='{$fila->estado}'>
					<span>{$fila->respuesta}</span>
					<br>
					<a href='https://{$fila->fuente}' target='_blank'>{$fila->fuente}</a>
					
					<div class='reply-data'>
						<span class='{$classstar}' data-idusuariorespuesta='{$fila->idusuario}' data-idrespuesta='{$fila->idrespuesta}'></span>
						<span href='' class='reply-user-name'>{$fila->usuario}</span>
						<span class='reply-date'>respondió el {$fecha}</span>
					</div>
				</div></div><hr>";
			

			
		}
	}

	if ($_GET['operacion'] == 'respuestapregunta') {

		$objPreguntasModel->RespuestaPregunta($_GET['idpregunta'], $_GET['idusuario'], filter_var($_GET['respuesta'], FILTER_SANITIZE_SPECIAL_CHARS), $_GET['fuente']);

	}

// POR AHORA ESTO NO LO LLAMO
	if ($_GET['operacion'] == 'crearpregunta') {

		$objPreguntasModel->CrearPregunta($_GET['idusuario'], $_GET['idcarrera'], $_GET['pregunta'], $_GET['textoRTF'], $_GET['fuente'], $_GET['dificultad']);

	}

	if ($_GET['operacion'] == 'mejorrespuesta') {

		$objPreguntasModel->MarcarMejorRespuesta($_GET['idpregunta'], $_GET['idrespuesta'], $_GET['idusuariorespuesta']);

	}
}

if (isset($_POST['question']))
{
	$idusuario = $_SESSION['id'];
	$idcarrera = $_SESSION['idcarrera'];
	$question = $_POST['question'];
	$newquestion = $_POST['newquestion'];
	$link = $_POST['link'];
	$dificultad = $_POST['dificultad'];

	$numeroinput = $_POST['numeroinput'];

	if ($link == "") {
		$link = null;
	}

	if ($question != "" && $newquestion != "") {
		$data = $objPreguntasModel->CrearPregunta($idusuario, $idcarrera, filter_var($question, FILTER_SANITIZE_SPECIAL_CHARS), $newquestion, $link, $dificultad);

		$responseIdpregunta = $data->_idpregunta;
		if ($numeroinput>0) {
			for ($i=0; $i < $numeroinput; $i++) { 
				
				if (isset($_FILES['imagen'.$i])) {
					if ($_FILES['imagen'.$i]['error'] == 0) {
						$imagenFor = $_FILES['imagen'.$i];

						$namearchivo = date("H-i-s");
						$tmp_name = $_FILES['imagen'.$i]["tmp_name"];
						$name = basename($_FILES['imagen'.$i]["name"]);
						$ruta = "../views/image/subidas_users/".$namearchivo.$name;
						move_uploaded_file($tmp_name, $ruta);

						$objPreguntasModel->AgregarImagenPregunta($responseIdpregunta, $ruta);
						echo "0";
					}else{
						echo "1";
					}
				}

			}
		}
	}else{
		echo "2";
	}
}






?>