<?php
session_start();
require_once "../models/personas.model.php";
require_once "../models/personas.entidad.php";

$objPersonasModel = new PersonasModel();

if (isset($_GET['operacion']))
{
	if ($_GET['operacion'] == 'registrarpersona') {

		$data = $objPersonasModel->RegistrarPersona($_GET['apellidos'], $_GET['nombres'], $_GET['dni'], $_GET['fechanac'], filter_var( $_GET['email'], FILTER_SANITIZE_EMAIL), $_GET['telefono']);

		echo $data->idpersona;

		$idnewpersona = $data->idpersona;
		$_SESSION['idnewpersona'] = $idnewpersona;
	}

	if ($_GET['operacion'] == 'registrausuario') {
		$passhash = password_hash($_GET['clave'], PASSWORD_DEFAULT);

		$data = $objPersonasModel->RegistraUsuario($_GET['idpersona'], $_GET['idcarrera'], $_GET['idtipo'], $_GET['usuario'], $passhash, $_GET['item_perfil']);

		echo $data->idusuario;
	}

	if ($_GET['operacion'] == 'topten') {

		$tabla = $objPersonasModel->topten();
		$nivel = "";
		$item_nivel = "";
		$contador = 1;

		echo "<h2>TOP 10</h2><hr>";

		foreach ($tabla as $fila)
		{
			if ($fila->puntosAcumulados >= 0 and $fila->puntosAcumulados < 50) {
				$nivel = "Pollito";
				$item_nivel = "pollito";
			}
			if ($fila->puntosAcumulados > 50 and $fila->puntosAcumulados < 125) {
				$nivel = "Cachimbo";
				$item_nivel = "cachimbo";
			}
			if ($fila->puntosAcumulados > 125 and $fila->puntosAcumulados < 200) {
				$nivel = "Basico";
				$item_nivel = "basico";
			}
			if ($fila->puntosAcumulados > 200 and $fila->puntosAcumulados < 300) {
				$nivel = "Practicante";
				$item_nivel = "practicante";
			}
			if ($fila->puntosAcumulados > 300 and $fila->puntosAcumulados < 400) {
				$nivel = "Graduado";
				$item_nivel = "graduado";
			}
			if ($fila->puntosAcumulados > 400 and $fila->puntosAcumulados < 550) {
				$nivel = "Medalla de Bronce";
				$item_nivel = "m_plata";
			}
			if ($fila->puntosAcumulados > 550 and $fila->puntosAcumulados < 800) {
				$nivel = "Medalla de Plata";
				$item_nivel = "m_plata";
			}
			if ($fila->puntosAcumulados > 800 and $fila->puntosAcumulados < 1000) {
				$nivel = "Medalla de Oro";
				$item_nivel = "m_oro";
			}
			if ($fila->puntosAcumulados > 1000) {
				$nivel = "Medalla de Diamante";
				$item_nivel = "m_diamante";
			}

			if ($contador != 1) {
				echo "<hr>";
			}

			echo "<div class='row'>
					<div class='col-sm-1'>
						<h5 style='margin-top: 1.5em;'>{$contador}</h5>
					</div>
					<div class='col-sm-3'>
						<img src='image/perfiles_user/{$fila->item_perfil}'>
					</div>
					<div class='col-sm-6'>
						<h5><strong>{$fila->persona}</strong></h5>
						<span>Nivel: {$nivel}</span>
<br>					<span>{$fila->puntosAcumulados} puntos</span>
					</div>
					<div class='col-sm-2'>
						<img class='img-level' src='image/niveles/{$item_nivel}.png'>
					</div>
				</div>";

			$contador++;
		}
	}

	if ($_GET['operacion'] == 'actualizaritemperfil') {
		
		$objPersonasModel->ActualizarItemPerfil($_GET['idusuario'], $_GET['item_perfil']);

	}

	if ($_GET['operacion'] == 'actualizardatauser') {
		
		$objPersonasModel->AcualizarUsuario($_GET['idpersona'], $_GET['apellidos'], $_GET['nombres'], $_GET['dni'], $_GET['fechanac'], $_GET['email'], $_GET['telefono']);

	}

	if ($_GET['operacion'] == 'subirniveluser') {

		$data = $objPersonasModel->ObtenerUsuario($_GET['idusuario']);
		$objPersonasModel->SubirNivelUser($_GET['idusuario'], $data->puntosAcumulados);

	}

}

if (isset($_POST['idusuario']))
{

	$idusuario = $_POST['idusuario'];

	$namearchivo = date("H-i-s");
	$tmp_name = $_FILES['itemperfil']["tmp_name"];
	$name = basename($_FILES['itemperfil']["name"]);
	$nombrearchivo = $namearchivo.$name;
	$ruta = "../views/image/perfiles_user/".$namearchivo.$name;
	move_uploaded_file($tmp_name, $ruta);

	$objPersonasModel->ActualizarItemPerfil($idusuario, $nombrearchivo);
}




?>