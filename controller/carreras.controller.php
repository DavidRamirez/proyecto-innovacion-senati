<?php

require_once "../models/carreras.model.php";
require_once "../models/carreras.entidad.php";

$objCarrerasModel = new CarrerasModel();

if (isset($_GET['operacion']))
{
	if ($_GET['operacion'] == 'getidcarrera') {
		session_start();
		$constidcarrera = $_GET['idcarrera'];

		$_SESSION['newidcarrera'] = $constidcarrera;

		echo $_SESSION['newidcarrera'];
	}
	if ($_GET['operacion'] == 'listarfamilias')
	{
		$tabla = $objCarrerasModel->listarFamilias();

		foreach ($tabla as $fila)
		{
			echo "<a class='link-card card-familia' data-idfamilia='{$fila->idfamilia}' data-nombrefamilia='{$fila->nombrefamilia}'>
					<div class='tarjeta' style='width: 18rem;''>
						<div class='card-body' style='background-image: url(./views/image/familias/{$fila->idfamilia}.jpeg)'>
							<h3 class='card-title'>{$fila->nombrefamilia}</h3>
						</div>
					</div>
				</a>";
		}
	}

	if ($_GET['operacion'] == 'listarcarreras')
	{
		$tabla = $objCarrerasModel->listarCarreras($_GET['idfamilia']);

		foreach ($tabla as $fila)
		{
			echo "<a class='link-card card-carreras' data-idcarrera='{$fila->idcarrera}'>
					<div class='tarjeta' style='width: 18rem;''>
						<div class='card-body' style='background-image: url(./views/image/familias/{$fila->idfamilia}.jpeg)'>
							<h3 class='card-title'>{$fila->nombrecarrera}</h3>
						</div>
					</div>
				</a>";
		}
	}
}

?>