<?php

require_once "../models/questions.model.php";
require_once "../models/questions.entidad.php";

$objPreguntasModel = new PreguntasModel();

if (isset($_GET['operacion']))
{
	if ($_GET['operacion'] == 'listarFamilia')
	{

		$tabla = $objPreguntasModel->ListaPreguntaFamilia($_GET['idfamilia']);

		foreach ($tabla as $fila)
		{
			$closequestion = "";
			if ($fila->estado == '0') {
				$closequestion = "<span id='close-question' class='fas fa-check-square'></span> [Resuelto] - ";
			}

			$date = date_create($fila->fechahoraedicion);
			$fechaformato = date_format($date, 'd-m-Y H:i:s');
			$fecha = substr($fechaformato, 0, 10);
			$dificultad = "";

			if ($fila->dificultadfacil == '1') {
				$dificultad = "flag-green";
			}
			if ($fila->dificultadmedio == '1') {
				$dificultad = "flag-orange";
			}
			if ($fila->dificultadhard == '1') {
				$dificultad = "flag-red";
			}

			echo "<div class='card'>
					<div class='card-body'>
						<span>{$fecha}</span>
						<h5 class='card-title'>{$closequestion}{$fila->pregunta}</h5>
						<p class='card-text'>{$fila->nombrecarrera}</p>
						<span class='fas fa-flag {$dificultad}'></span>
						<a href='#' id='question' class='btn btn-info' data-idpregunta='{$fila->idpregunta}' data-idusuario='{$fila->idusuario}'>Ver respuestas</a>
					</div>
				  </div>";
				 // href='../views/questions.php'
		}
	}

	if ($_GET['operacion'] == 'listarCarrera')
	{

		$tabla = $objPreguntasModel->ListaPreguntaCarrera($_GET['idcarrera']);

		foreach ($tabla as $fila)
		{
			$closequestion = "";
			if ($fila->estado == '0') {
				$closequestion = "<span id='close-question' class='fas fa-check-square'></span> [Resuelto] - ";
			}

			$date = date_create($fila->fechahoraedicion);
			$fechaformato = date_format($date, 'd-m-Y H:i:s');
			$fecha = substr($fechaformato, 0, 10);
			$dificultad = "";

			if ($fila->dificultadfacil == '1') {
				$dificultad = "flag-green";
			}
			if ($fila->dificultadmedio == '1') {
				$dificultad = "flag-orange";
			}
			if ($fila->dificultadhard == '1') {
				$dificultad = "flag-red";
			}

			echo "<div class='card'>
					<div class='card-body'>
						<span>{$fecha}</span>
						<h5 class='card-title'>{$closequestion}{$fila->pregunta}</h5>
						<p class='card-text'>{$fila->nombrecarrera}</p>
						<span class='fas fa-flag {$dificultad}'></span>
						<a href='#' id='question' class='btn btn-info' data-idpregunta='{$fila->idpregunta}' data-idusuario='{$fila->idusuario}'>Ver respuestas</a>
					</div>
				  </div>";
				 // href='../views/questions.php'
		}
	}

	if ($_GET['operacion'] == 'listarTexto')
	{

		$tabla = $objPreguntasModel->ListaPreguntaTexto($_GET['idfamilia'], $_GET['valor']);

		foreach ($tabla as $fila)
		{
			$closequestion = "";
			if ($fila->estado == '0') {
				$closequestion = "<span id='close-question' class='fas fa-check-square'></span> [Resuelto] - ";
			}
			$date = date_create($fila->fechahoraedicion);
			$fechaformato = date_format($date, 'd-m-Y H:i:s');
			$fecha = substr($fechaformato, 0, 10);
			$dificultad = "";

			if ($fila->dificultadfacil == '1') {
				$dificultad = "flag-green";
			}
			if ($fila->dificultadmedio == '1') {
				$dificultad = "flag-orange";
			}
			if ($fila->dificultadhard == '1') {
				$dificultad = "flag-red";
			}

			echo "<div class='card'>
					<div class='card-body'>
						<span>{$fecha}</span>
						<h5 class='card-title'>{$closequestion}{$fila->pregunta}</h5>
						<p class='card-text'>{$fila->nombrecarrera}</p>
						<span class='fas fa-flag {$dificultad}'></span>
						<a href='#' id='question' class='btn btn-info' data-idpregunta='{$fila->idpregunta}' data-idusuario='{$fila->idusuario}'>Ver respuestas</a>
					</div>
				  </div>";
				 // href='../views/questions.php'
		}
	}

	if ($_GET['operacion'] == 'idnewpregunta')
	{
		$newidpregunta = $_GET['idpregunta'];
		$newidusuario = $_GET['idusuario'];
		
		session_start();
		$_SESSION['newidpregunta'] = $newidpregunta;
		$_SESSION['newidusuario'] = $newidusuario;

		echo $_SESSION['newidpregunta'];
		echo $_SESSION['newidusuario'];
	}
}

?>