<?php

require_once "../models/login.model.php";

$objLogin = new LoginModel();

function CalculaEdad( $fecha ) {
    list($Y,$m,$d) = explode("-",$fecha);
    return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
}

if (isset($_GET['operacion'])) {
	if ($_GET['operacion'] == 'verificarlogeo') {
		echo $objLogin->verificarlogeo($_GET['usuario'], $_GET['clave']);
	}


	if ($_GET['operacion'] == 'destruirsession') {
		echo $objLogin->CerrarSesion();
	}

	if ($_GET['operacion'] == 'obtenerusuario') {
		$data = $objLogin->DataUsuario('1');

		$edad = CalculaEdad($data->fechanac);

		echo "
			<div class='col-sm-12 col-md-4 col-lg-2 edit-user-container'>

				<div id='edit-img-user'>
					<img class='img-user' src='image/perfiles_user/{$data->item_perfil}' data-idusuario='{$_SESSION['id']}'>
				</div>

				<button type='button' class='btn btn-primary btn-edit-user' data-toggle='modal' data-target='#modalEditUserItem'><span class='fas fa-edit'></span></button>

				<h4 class='name-user'><strong>{$data->persona}</strong></h4>

				<p class='email-user'><a href='#' data-toggle='tooltip' data-placement='top' title='Copiar correo'>{$data->email}</a></p>

				<p class='points-user'>{$data->puntosAcumulados} puntos acumulados</p>

				<div class='btn-edit-user-all'>
					<button id='edituserPerfil' type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalEditUser'>Editar Perfil</button>
				</div>
			</div>


			<div class='col-sm-12 col-md-1 col-lg-1'></div>


			<div class='col-sm-12 col-md-7 col-lg-6 margin-top'>
				<div class='title-col-2'>
					<h3><strong>{$data->nombre}</strong></h3>
					<img class='img-nivel' src='image/niveles/{$data->avatar}.png'>
				</div>
				<div class='contend-col-2'>
					<p>{$data->nombrecarrera}</p>
				</div>
			</div>


			<div class='col-sm-12 col-md-12 col-lg-3 margin-top'>
				<div class='title-col-2'>
					<h3><strong>Tus ultimas Preguntas</strong></h3>
				</div>
				<div class='end-questions'>
					
				</div>
				
			</div>
			";
	}

	if ($_GET['operacion'] == 'obtenerdatausuario') {
		$data = $objLogin->DataUsuario('1');

		echo json_encode($data);
	}
}





?>