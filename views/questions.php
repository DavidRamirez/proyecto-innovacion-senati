<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Preguntas</title>

	<link rel="stylesheet" href="lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="lib/icons/css/all.min.css">
	<link rel="stylesheet" href="lib/css/sweetalert2.min.css">
	<link rel="stylesheet" href="lib/css/jquery.fancybox.min.css">
	<link rel="stylesheet" href="lib/css/nav.css">
	<link rel="stylesheet" href="lib/css/questions.css">

</head>
<body>
	<nav class="navbar fixed-top navbar-expand-sm">
		<a href="index.php" class="navbar-brand"><img src="image/logos/conversation_32.png">Bienvenido</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="fas fa-bars"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<div class="float-right">
				<button type="button" id="notificacion" class="btn btn-outline-info float-rigth" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom" data-content="
				<div class='float-notificacion'>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div>
				</div>
				">
					<span class="fas fa-bell"></span>
				</button>

				<button class="btn btn-outline-primary dropdown-toggle float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img id="img-user" class="user-nav" <?php echo "src='image/perfiles_user/".$_SESSION['item_perfil']."'"; ?>>
				</button>
				<div class="dropdown-menu dropdown-menu-lg-right" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="index.php">Inicio</a>
					<a class="dropdown-item" href="user.php">Perfil</a>
					<a class="dropdown-item" href="newQuestions.php">Crear Pregunta</a>
					<div class="dropdown-divider"></div>
					<a id="closeSession" class="dropdown-item" href="#">Cerrar Sesión</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="margin-top-nav"></div>


	<div class="container" id="container-question" <?php echo "data-idpregunta='{$_SESSION['newidpregunta']}'"; echo "data-idusuario='{$_SESSION['id']}'"; echo "data-preguntaidusuario='{$_SESSION['newidusuario']}'"; ?>>
		<div class="question" id="row-question">
			<!-- PREGUNTA CARGARA POR AJAX OBTENIENDO ID -->
		</div>
<hr class="end-question">

		<div id="reply-question">
			<!-- LAS REPUESTAS CARGADAS POR AJAX -->
		</div>

	</div>
	<br><br><br><br>
</body>
</html>

<script src="lib/js/jquery-3.4.1.min.js"></script>
<script src="lib/js/popper.min.js"></script>
<script src="lib/js/bootstrap.min.js"></script>
<script src="lib/js/sweetalert2.min.js"></script>
<script src="lib/js/jquery.fancybox.min.js"></script>
<script src="lib/js/sessiones.js"></script>
<script src="lib/js/question.js"></script>