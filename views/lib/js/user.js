$(document).ready(function(){
	$('#notificacion').popover({
		html: true
	});

	var idusuario = "";
	var item_perfil = "";

	$(".custom-file-label").click(function(){
		$("#itemperfil").click();
	})

	$(".custom-file-input").change(function(){
		var fileName = $(this).val().split("\\").pop();
		$(this).siblings(".custom-file-label").addClass("selected").html(fileName);

		var fileInput = $(this)[0];
		var filePath = fileInput.value;
		var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
		if(!allowedExtensions.exec(filePath)){
			Swal.fire(
				'Error en el formato',
				'Solo puede ingresar imagenes',
				'error'
			)
			$("#testinput").val('');
			fileInput.value = '';
			return false;
		}else{
			if (fileInput.files && fileInput.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					document.getElementById('img-test').innerHTML = "<img class='custom-file-label' id=img-data src='"+e.target.result+"'/>";

					$("#testinput").val('1');
				};
				reader.readAsDataURL(fileInput.files[0]);
			}
			$("#testinput").val('');
		}
	});

	$("#fechanac").click(function(){
		$(this).get(0).type = "date";
	});
	$("#fechanac").focus(function(){
		$(this).get(0).type = "date";
	});

	$("#nombre").keypress(function (key) {
		if ((key.charCode < 97 || key.charCode > 122)//letras mayusculas
		&& (key.charCode < 65 || key.charCode > 90) //letras minusculas
		&& (key.charCode != 45) //retroceso
		&& (key.charCode != 241) //ñ
		&& (key.charCode != 209) //Ñ
		&& (key.charCode != 32) //espacio
		&& (key.charCode != 225) //á
		&& (key.charCode != 233) //é
		&& (key.charCode != 237) //í
		&& (key.charCode != 243) //ó
		&& (key.charCode != 250) //ú
		&& (key.charCode != 252) //ü
		&& (key.charCode != 193) //Á
		&& (key.charCode != 201) //É
		&& (key.charCode != 205) //Í
		&& (key.charCode != 211) //Ó
		&& (key.charCode != 218) //Ú
		)
		return false;

	});

	$("#apellido").keypress(function (key) {
		if ((key.charCode < 97 || key.charCode > 122)//letras mayusculas
		&& (key.charCode < 65 || key.charCode > 90) //letras minusculas
		&& (key.charCode != 45) //retroceso
		&& (key.charCode != 241) //ñ
		&& (key.charCode != 209) //Ñ
		&& (key.charCode != 32) //espacio
		&& (key.charCode != 225) //á
		&& (key.charCode != 233) //é
		&& (key.charCode != 237) //í
		&& (key.charCode != 243) //ó
		&& (key.charCode != 250) //ú
		&& (key.charCode != 252) //ü
		&& (key.charCode != 193) //Á
		&& (key.charCode != 201) //É
		&& (key.charCode != 205) //Í
		&& (key.charCode != 211) //Ó
		&& (key.charCode != 218) //Ú
		)
		return false;

	});

	$("#dni").keypress(function(key){
		if (key.charCode < 49 || key.charCode > 57)
			return false;
	});

	$("#celular").keypress(function(key){
		if (key.charCode < 49 || key.charCode > 57)
			return false;
	});

	$("#modal-item").on("click", ".select-item", function(){
		$(".select-item").removeClass("select-item-selected");
		$(this).addClass("select-item-selected");
		item_perfil = $(this).attr('reply_512');
		$("#update-perfil").removeAttr("disabled");
	});

	$("#cancel-perfil").click(function(){
		$(".select-item").removeClass("select-item-selected");
		$("#update-perfil").attr("disabled", "true");
	});

	function ListarUltimasPreguntas(idusuario){
		$.ajax({
			url: '../controller/question.controller.php',
			type: 'GET',
			data: 'operacion=listarPreguntaUsuario&idusuario=' + idusuario,
			success:function(e){
				if (e == "") {
					$(".end-questions").html("Aun no hay preguntas");
				}else{
					$(".end-questions").html(e);
				}
			}
		});
	}
	function ListarDataUser(){
		$.ajax({
			url: '../controller/login.controller.php',
			type: 'GET',
			data: 'operacion=obtenerusuario',
			success:function(e){
				$("#user-data").html(e);
			}
		});
	}

	ListarDataUser();

	setTimeout(function(){
		idUsuario = $(".img-user").attr("data-idusuario");
		ListarUltimasPreguntas(idUsuario);
	},1000);

	$("#user-data").on("click", "#edituserPerfil", function(){
		$.ajax({
			url: '../controller/login.controller.php',
			type: 'GET',
			data: 'operacion=obtenerdatausuario',
			success:function(e){
				var data = JSON.parse(e);

				$("#edit-user").attr("data-idpersona", data.idpersona);
				$("#nombre").val(data.nombres);
				$("#apellido").val(data.apellidos);
				$("#dni").val(data.dni);
				$("#celular").val(data.telefono);
				$("#fechanac").val(data.fechanac);
				$("#email").val(data.email);

			}
		});
	});

	$("#guardarcambiosuser").click(function(){

		var datosuser = {
			'operacion': 'actualizardatauser',
			'idpersona': $("#edit-user").attr("data-idpersona"),
			'apellidos': $("#apellido").val(),
			'nombres': $("#nombre").val(),
			'dni': $("#dni").val(),
			'fechanac': $("#fechanac").val(),
			'email': $("#email").val(),
			'telefono': $("#celular").val()
		}

		$.ajax({
			url: '../controller/persona.controller.php',
			type: 'GET',
			data: datosuser,
			success:function(e){
				$("#modalEditUser").modal("toggle");
				ListarDataUser();
				ListarUltimasPreguntas(idUsuario);
			}
		});

	});

	$("#update-perfil").click(function(evento){
		evento.preventDefault();

		$("#idusuario").val(idUsuario);

		var dataItem = {
			'operacion': 'actualizaritemperfil',
			'idusuario': idUsuario,
			'item_perfil': $(".select-item-selected").attr('data-item')
		}

		var dataImg = new FormData($("#form-imagen")[0]);

		if ($("#testinput").val() == '1') {
			$.ajax({
				url: '../controller/persona.controller.php',
				type: 'POST',
				data: dataImg,
				contentType: false,
				processData: false,
				success: function(e){
					ListarDataUser();
					ListarUltimasPreguntas(idUsuario);
					$("#modalEditUserItem").modal("toggle");
				}
			});
		}else{
			$.ajax({
				url: '../controller/persona.controller.php',
				type: 'GET',
				data: dataItem,
				success: function(e){
					ListarDataUser();
					ListarUltimasPreguntas(idUsuario);
					$("#modalEditUserItem").modal("toggle");
				}
			});
		}

		
	})

});