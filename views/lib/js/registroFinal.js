$(document).ready(function(){


	var item_perfil = "";

	$("#modal-item").on("click", ".select-item", function(){
		item_perfil = $(this).attr("data-item");
		val_item = "views/image/perfiles_user/"+ item_perfil +".png";

		$("#img-test").removeAttr('src');
		$("#img-test").attr('src', val_item);
	});

	$("#crearCuenta").click(function(){
		var idpersona = $("#form-usuario").attr('data-idpersona');
		var idcarrera = $("#form-usuario").attr('data-idcarrera');
		var idtipo = '3';
		var usuario = $("#usuario").val();
		var password = $("#pass").val();
		var password2 = $("#pass2").val();

		if (password === password2) {
			var datos = {
				'operacion': 'registrausuario',
				'idpersona': idpersona,
				'idcarrera': idcarrera,
				'idtipo': idtipo,
				'usuario': usuario,
				'clave': password2,
				'item_perfil': item_perfil
			}

			$.ajax({
				url: './controller/persona.controller.php',
				type: 'GET',
				data: datos,
				success:function(e){
					console.log(e);

					if (e != 0) {

						var datoslogin = {
							'operacion': 'verificarlogeo',
							'usuario': usuario,
							'clave': password2
						}

						$.ajax({
							url: './controller/login.controller.php',
							type: 'GET',
							data: datoslogin,
							success:function(e){
								console.log(e);
								if (e == '1') {
									window.location.href = "views/index.php";
								}
							}
						});
					}
				}
			});

		}else{
			Swal.fire({
				title: 'Error de datos',
				type: 'error',
				text: "Las contraseñas no son iguales"
			});
		}

	});

});