CKEDITOR.replace('newquestion');

var contadorImput = 0;
var contadorTodoImput = 0;
var descontadorImput = 0;
var ultimoInput = "";
var contadorclick = 0;

function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}

$("#crearImput").on("change", ".custom-file-input", function() {
	var fileInput = $(this)[0];

	var filePath = fileInput.value;
	var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
	if(!allowedExtensions.exec(filePath)){
		Swal.fire(
			'Error en el formato',
			'Solo puede ingresar imagenes',
			'error'
		)
		fileInput.value = '';
		return false;
	}else{
		if (fileInput.files && fileInput.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				document.getElementById('img-test').innerHTML = '<img id=img-data src="'+e.target.result+'"/>';

				console.log(fileInput.id);
				console.log("customFile"+contadorImput);

				if (fileInput.id == "customFile"+contadorImput) {
					$("#btn-add-inputFile").click();
					console.log(contadorImput+1);
				}
				
			};
			reader.readAsDataURL(fileInput.files[0]);
		}
	}

    var fileName = $(this).val().split("\\").pop();
	$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$("#crearImput").on("click", ".btn-delete-img", function(){
	var idImput = $(this).attr("data-idInput");

	$("#"+idImput).attr("name", "imagen");
	$("."+idImput).addClass("display-none");

	if ("customFile"+contadorImput == idImput) {
		contadorImput--;
	}

});

$("#btn-add-inputFile").click(function(){
	contadorImput++;
	contadorTodoImput++;
 
	var padre = document.getElementById("crearImput");
	var divCustom = document.createElement("div");
	var divRow = document.createElement("div");
	var divCol7 = document.createElement("div");
	var divCol1 = document.createElement("div");
	var button = document.createElement("button");
	var span = document.createElement("span");
	var input = document.createElement("input");
	var label = document.createElement("label");

	divCustom.setAttribute("class", "custom-file customFile"+contadorImput);
	divRow.setAttribute("class", "row");
	divCol7.setAttribute("class", "col-sm-7");
	divCol1.setAttribute("class", "col-sm-1");

	button.setAttribute("id", "close"+contadorImput);
	button.setAttribute("data-numer", contadorImput);
	button.setAttribute("type", "button");
	button.setAttribute("class", "btn float-right btn-delete-img");
	button.setAttribute("data-idInput", "customFile"+contadorImput);

	span.setAttribute("class", "fas fa-times-circle");

	input.setAttribute("type", "file");
	input.setAttribute("name", "imagen"+contadorImput);
	input.setAttribute("class", "custom-file-input");
	input.setAttribute("id", "customFile"+contadorImput);
	input.setAttribute("data-estado", "1");

	label.innerHTML = 'Máximo 2 megas';
	label.setAttribute("class", "custom-file-label");

	padre.appendChild(divCustom);
	divCustom.appendChild(divRow);
	divRow.appendChild(divCol7);
	divRow.appendChild(divCol1);
	divCol7.appendChild(input);
	divCol7.appendChild(label);
	divCol1.appendChild(button);
	button.appendChild(span);


	ultimoInput = "customFile"+contadorImput;
});

$("#send-question").click(function(evento){
	evento.preventDefault();

	Swal.fire({
		type: 'question',
		title: '¿Que tan complicada es la pregunta?',
		html: '<div id="container-difficulty"><span class="far fa-flag flag-green difficulty" data-valor="1"></span> <span class="far fa-flag flag-orange difficulty" data-valor="2"></span> <span class="far fa-flag flag-red difficulty" data-valor="3"></span></div>',
		showConfirmButton: false,
		allowOutsideClick: false
	})

	$("#container-difficulty").on("click", ".difficulty", function(){
		CKupdate();

		$(".difficulty").removeClass("fas").addClass("far");
		$(this).removeClass("far").addClass("fas");

		var dificultad = $(this).attr("data-valor");

		$("#dificultad").val(dificultad);
		$("#numeroinput").val(contadorTodoImput);

		var dataImg = new FormData($("#form-imagen")[0]);

		if (contadorclick == 0) {
			$.ajax({
				url: '../controller/question.controller.php',
				type: 'POST',
				data: dataImg,
				contentType: false,
				processData: false,
				success: function(e){
					console.log(e);
					if (e == 0) {
						setTimeout(function(){
							swal.close();
							window.location.href = "./index.php";
						},1);
					}else if(e == 1){
						Swal.fire({
							type: 'error',
							title: 'La imagen es muy pesada, selecciona otra imagen'
						})
						contadorclick = 0;
					}else if(e == 2){
						Swal.fire({
							type: 'error',
							title: 'Ingresa una pregunta y una descripcion valida'
						})
						contadorclick = 0;
					}
					$("#rpta").html(e);
				}
			});
		}

		contadorclick++;

	});
});