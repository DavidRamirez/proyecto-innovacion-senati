$(document).ready(function(){

function logear($usuario, $clave){

	var datos = {
		'operacion': 'verificarlogeo',
		'usuario': $usuario,
		'clave': $clave
	}
	
	$.ajax({
		url: './controller/login.controller.php',
		type: 'GET',
		data: datos,
		success:function(e){
			if (e == '1') {
				window.location.href = "views/index.php";
			}else{
				Swal.fire(
					'Algo mal pasó',
					'Usuario o clave incorrecto',
					'error'
				)
			}
		}
	});
}

$("#IniciarSession").click(function(){
	var usuario = $("#usuario").val();
	var pass = $("#pass").val();

	logear(usuario, pass);
});

$("#pass").keypress(function(){
	var keycode = event.keyCode || event.which;
	if(keycode == 13){
		var usuario = $("#usuario").val();
		var pass = $("#pass").val();

		logear(usuario, pass);
	}
});

});