var idnewpersona = $("#card-familias").attr("data-idpersona");
var constidFamilia = "";
var constnombreFamilia = "";

function listarfamilias(){
	$.ajax({
		url: './controller/carreras.controller.php',
		type: 'GET',
		data: 'operacion=listarfamilias',
		success:function(e){
			$("#card-familias").html(e);
		}
	});
}

function listarcarreras(idfamilia){
	$.ajax({
		url: './controller/carreras.controller.php',
		type: 'GET',
		data: 'operacion=listarcarreras&idfamilia='+idfamilia,
		success:function(e){
			$("#card-familias").html(e);
		}
	});
}

listarfamilias();

$("#card-familias").on("click", ".card-familia", function(){
	constidFamilia = $(this).attr("data-idfamilia");
	constnombreFamilia = $(this).attr("data-nombrefamilia");

	listarcarreras(constidFamilia);

	$(".title").html("<strong>CARRERAS</strong> de");
	$(".sub-title").attr("style", "margin-left: 32px;");
	$(".sub-title").html("<strong>"+ constnombreFamilia +"</strong>");
	$("#btn-back").addClass("showbtnback");
	$("#btn-back").removeClass("hidebtnback");
});

$("#card-familias").on("click", ".card-carreras", function(){
	var idcarrera = $(this).attr("data-idcarrera");

	console.log(idcarrera);

	$.ajax({
		url: './controller/carreras.controller.php',
		type: 'GET',
		data: 'operacion=getidcarrera&idcarrera='+idcarrera,
		success:function(e){
			if (e>0) {
				window.location.href = "./registroFinal.php";
			}
		}
	});
});

$("#btn-back").click(function(){
	listarfamilias();

	$(".title").html("<strong>FAMILIAS OCUPACIONALES</strong>");
	$(".sub-title").removeAttr("style");
	$(".sub-title").html("<strong>SENATI</strong>");
	$("#btn-back").addClass("hidebtnback");
	$("#btn-back").removeClass("showbtnback");
});
