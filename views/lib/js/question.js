$( document ).ready(function() {
	$('#notificacion').popover({
		html: true
	});
	var bestreply=0;

	var idPregunta = $("#container-question").attr("data-idpregunta");
	var idUsuario = $("#container-question").attr("data-idusuario");
	var preguntaidUsuario = $("#container-question").attr("data-preguntaidusuario");

	function ListarPregunta(idpregunta){
		$.ajax({
			url: '../controller/question.controller.php',
			type: 'GET',
			data: 'operacion=listarPregunta&idpregunta=' + idpregunta,
			success:function(e){
				$("#row-question").html(e);
			}
		});
	}
	ListarPregunta(idPregunta);

	function ListarRespuestas(idpregunta){
		$.ajax({
			url: '../controller/question.controller.php',
			type: 'GET',
			data: 'operacion=listarRespuestas&idpregunta=' + idpregunta,
			success:function(e){
				$("#reply-question").html(e);
				$("#user-reply").val('');
				$("#link-reply").val('');
				ListarPregunta(idPregunta);
			}
		});
	}
	ListarRespuestas(idPregunta);

	$("#row-question").on("click", "#enviar", function(){

		var reply = $("#user-reply").val();
		var link = $("#link-reply").val();

		if (reply != "") {

			if (link == "") { link = null; }

			var datos ={
				"operacion" : "respuestapregunta",
				"idpregunta" : idPregunta,
				"idusuario" : idUsuario,
				"respuesta" : reply,
				"fuente" : link
			};

			var datosuser = {
				"operacion" : "subirniveluser",
				"idusuario" : idUsuario
			};

			$.ajax({
				url: '../controller/question.controller.php',
				type: 'GET',
				data: datos,
				success:function(e){
					$.ajax({
						url: '../controller/persona.controller.php',
						type: 'GET',
						data: datosuser,
						success:function(e){

						}
					});
					ListarRespuestas(idPregunta);
				}
			});
		}
	});

	// PARA MARCAR LA MEJOR RESPUESTA
	$("#reply-question").on("click", ".best-reply", function(){
		var idusuarioRespuesta = $(this).attr("data-idusuariorespuesta");
		var idrespuesta = $(this).attr("data-idrespuesta");

		var estadorespuesta = $(".reply-contend").attr("data-estado");
		var estadopregunta = $(".question-user").attr("data-estadopregunta");

		console.log(idUsuario);
		console.log(idrespuesta);
		console.log(preguntaidUsuario);

		if (preguntaidUsuario == idUsuario) {
			if (idusuarioRespuesta == preguntaidUsuario) {
				Swal.fire(
					'Escoge otra respuesta',
					'No puedes marcar tu respuesta como mejor respuesta',
					'warning'
				)
			}else if (preguntaidUsuario == idUsuario && estadopregunta != 0) {
				if ($(this).hasClass("far")) {
					if (bestreply==0) {
						var dataMejorRpta = {
							'operacion': 'mejorrespuesta',
							'idpregunta': idPregunta,
							'idrespuesta': idrespuesta,
							'idusuariorespuesta': idusuarioRespuesta
						};

						var datosuser = {
							"operacion" : "subirniveluser",
							"idusuario" : idusuarioRespuesta
						};

						const swalWithBootstrapButtons = Swal.mixin({
							customClass: {
									confirmButton: 'btn btn-success margin-left',
									cancelButton: 'btn btn-danger margin-right'
								},
							buttonsStyling: false
						})
						swalWithBootstrapButtons.fire({
							title: '¿Marcar como mejor respuesta?',
							text: "Al marcar la mejor respuesta, cerrará la pregunta",
							type: 'question',
							showCancelButton: true,
							confirmButtonText: ' Si',
							cancelButtonText: 'No',
							reverseButtons: true
						}).then((result) => {
							if (result.value) {
								swalWithBootstrapButtons.fire(
									'Correcto',
									'Se marco la mejor respuesta',
									'success'
								)

								$.ajax({
									url: '../controller/question.controller.php',
									type: 'GET',
									data: dataMejorRpta,
									success:function(e){
										$(this).removeClass("far").addClass("fas");
										bestreply++;
										ListarPregunta(idPregunta);
										ListarRespuestas(idPregunta);

										$.ajax({
											url: '../controller/persona.controller.php',
											type: 'GET',
											data: datosuser,
											success:function(e){
												
											}
										});
									}
								});
							}
						})
					}else{
						Swal.fire(
							'Ya tiene marcada la mejor respuesta',
							'',
							'warning'
						)
					}
				}else{
					$(this).removeClass("fas").addClass("far");
					bestreply=0;
				}
			}
		}
		
	});
})