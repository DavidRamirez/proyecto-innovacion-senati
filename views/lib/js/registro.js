$(document).ready(function(){
	$("#fechanac").click(function(){
		$(this).get(0).type = "date";
	});
	$("#fechanac").focus(function(){
		$(this).get(0).type = "date";
	});

	$("#nombre").keypress(function (key) {
		if ((key.charCode < 97 || key.charCode > 122)//letras mayusculas
		&& (key.charCode < 65 || key.charCode > 90) //letras minusculas
		&& (key.charCode != 45) //retroceso
		&& (key.charCode != 241) //ñ
		&& (key.charCode != 209) //Ñ
		&& (key.charCode != 32) //espacio
		&& (key.charCode != 225) //á
		&& (key.charCode != 233) //é
		&& (key.charCode != 237) //í
		&& (key.charCode != 243) //ó
		&& (key.charCode != 250) //ú
		&& (key.charCode != 252) //ü
		&& (key.charCode != 193) //Á
		&& (key.charCode != 201) //É
		&& (key.charCode != 205) //Í
		&& (key.charCode != 211) //Ó
		&& (key.charCode != 218) //Ú
		)
		return false;

	});

	$("#apellido").keypress(function (key) {
		if ((key.charCode < 97 || key.charCode > 122)//letras mayusculas
		&& (key.charCode < 65 || key.charCode > 90) //letras minusculas
		&& (key.charCode != 45) //retroceso
		&& (key.charCode != 241) //ñ
		&& (key.charCode != 209) //Ñ
		&& (key.charCode != 32) //espacio
		&& (key.charCode != 225) //á
		&& (key.charCode != 233) //é
		&& (key.charCode != 237) //í
		&& (key.charCode != 243) //ó
		&& (key.charCode != 250) //ú
		&& (key.charCode != 252) //ü
		&& (key.charCode != 193) //Á
		&& (key.charCode != 201) //É
		&& (key.charCode != 205) //Í
		&& (key.charCode != 211) //Ó
		&& (key.charCode != 218) //Ú
		)
		return false;

	});

	$("#dni").keypress(function(key){
		if (key.charCode < 49 || key.charCode > 57)
			return false;
	});

	$("#celular").keypress(function(key){
		if (key.charCode < 49 || key.charCode > 57)
			return false;
	});

	$("#close-modal").click(function(){
		$("#edit-user").reset[0];
	});


	$("#enviar").click(function(){
		var apellido = $("#apellido").val();
		var nombre = $("#nombre").val();
		var dni = $("#dni").val();
		var fechanac = $("#fechanac").val();
		var email = $("#email").val();
		var celular = $("#celular").val();

		var datos = {
			'operacion': 'registrarpersona',
			'apellidos': apellido,
			'nombres': nombre,
			'dni': dni,
			'fechanac': fechanac,
			'email': email,
			'telefono': celular
		};

		$.ajax({
			url: './controller/persona.controller.php',
			type: 'GET',
			data: datos,
			success:function(e){
				$("#test").html(e);
				$("#test").attr("data-idpersona",e);
				var idnewpersona = $("#test").attr("data-idpersona");


				if (e > 0) {
					Swal.fire({
						title: 'Muy Bien',
						type: 'success',
						text: "Se han registrado correctamente sus datos"
					}).then((result) => {
						window.location.href = "./familias.php";
					})
				}else{
					Swal.fire({
						title: 'Error de datos',
						type: 'info',
						text: "Por favor, verifique los datos ingresados"
					});
				}
			}
		});
			
	});
});