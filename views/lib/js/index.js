$(document).ready(function(){
	$('#notificacion').popover({
		html: true
	});

	var useridcarrera = $("#filtro").attr("data-useridcarrera");
	var useridfamilia = $("#filtro").attr("data-useridfamilia");
	var texto = "";

	function ListarTopTen(){
		$.ajax({
			url: '../controller/persona.controller.php',
			type: 'GET',
			data: 'operacion=topten',
			success:function(e){
				$(".recent").html(e + "<hr>");
			}
		});
	}
	ListarTopTen();

	function ListarPreguntasCarrera(idcarrera){
		$.ajax({
			url: '../controller/index.controller.php',
			type: 'GET',
			data: 'operacion=listarCarrera&idcarrera=' + idcarrera,
			success:function(e){
				$(".news").html(e);
			}
		});
	}

	function ListarPreguntasTexto(idfamilia, valor){
		var dataajax = {
			'operacion': 'listarTexto',
			'idfamilia': idfamilia,
			'valor': valor
		}

		$.ajax({
			url: '../controller/index.controller.php',
			type: 'GET',
			data: dataajax,
			success:function(e){
				$(".news").html(e);
			}
		});
	}

	function ListarPreguntasFamilia(idfamilia){
		$.ajax({
			url: '../controller/index.controller.php',
			type: 'GET',
			data: 'operacion=listarFamilia&idfamilia=' + idfamilia,
			success:function(e){
				$(".news").html(e);
			}
		});
	}
	ListarPreguntasFamilia(useridfamilia);

	$("#noticias").on("click", "#question", function(){
	
		var idnewpregunta = $(this).attr("data-idpregunta");
		var idnewusuario = $(this).attr("data-idusuario");

		var datos = {
			'operacion': 'idnewpregunta',
			'idpregunta': idnewpregunta,
			'idusuario': idnewusuario
		}

		$.ajax({
			url: '../controller/index.controller.php',
			type: 'GET',
			data: datos,
			success:function(e){
				console.log(e);
				window.location.href = "questions.php";
			}
		});
	
	});

	$("#filtro").on("click", ".btn", function(){

		$(".btn-filtro").removeClass('btn-warning');
		$(".btn-filtro").addClass('btn-outline-warning');

		$(this).removeClass('btn-outline-warning');
		$(this).addClass('btn-warning');

		if ($(this).attr("data-filtro") == "familia") {
			ListarPreguntasFamilia(useridfamilia);
		}
		if ($(this).attr("data-filtro") == "carrera") {
			ListarPreguntasCarrera(useridcarrera);
		}
	});

	$("#busqueda-pregunta").keypress(function(e){
		var code = (e.keyCode ? e.keyCode : e.which);

        if(code==13){
            $("#busqueda-btn").click();
        }
	})

	$("#busqueda-btn").click(function(){
		texto = $("#busqueda-pregunta").val();
		console.log(texto);
		if (texto != "") {
			ListarPreguntasTexto(useridfamilia, texto);
		}else{
			ListarPreguntasFamilia(useridfamilia);
		}
	});
});