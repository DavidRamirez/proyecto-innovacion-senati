<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Inicio</title>

	<link rel="stylesheet" href="lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="lib/icons/css/all.min.css">
	<link rel="stylesheet" href="lib/css/nav.css">
	<link rel="stylesheet" href="lib/css/index.css">
</head>
<body>
	<nav class="navbar fixed-top navbar-expand-sm">
		<a href="index.php" class="navbar-brand"><img src="image/logos/conversation_32.png">Bienvenido</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="fas fa-bars"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<div class="float-right">
				<button type="button" id="notificacion" class="btn btn-outline-info float-rigth" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom" data-content="
				<div id='float-notificacion'>
					<div><a href='#'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='#'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='#'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='#'>¿Como detener los servicios de windows update?</a>
					</div>
				</div>
				">
					<span class="fas fa-bell"></span>
				</button>

				<button class="btn btn-outline-primary dropdown-toggle float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img id="img-user" class="user-nav" <?php echo "src='image/perfiles_user/".$_SESSION['item_perfil']."'"; ?>>
				</button>
				<div class="dropdown-menu dropdown-menu-lg-right" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="#">Inicio</a>
					<a class="dropdown-item" href="user.php">Perfil</a>
					<a class="dropdown-item" href="newQuestions.php">Crear Pregunta</a>
					<div class="dropdown-divider"></div>
					<a id="closeSession" class="dropdown-item" href="#">Cerrar Sesión</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="margin-top-nav"></div>







	<div class="container">
		<div id="filtro" <?php echo "data-useridcarrera='{$_SESSION["idcarrera"]}'"; echo "data-useridfamilia='{$_SESSION["idfamilia"]}'"; ?>>
			<span class="filtro-text">Filtrar preguntas por</span>
			<button class="btn-filtro btn btn-warning" data-filtro="familia">Familias de Senati</button>
			<button class="btn-filtro btn btn-outline-warning" data-filtro="carrera">Carreras</button>
			<div class="form-inline">
				<input id="busqueda-pregunta" type="text" class="form-control" placeholder="Buscar Pregunta">
				<button id="busqueda-btn" class="btn btn-outline-warning" type="button"><span class="fas fa-search"></span></button>
			</div>
		</div>
		<div class="row">
			<div id="noticias" class="col-md-8 news">

				<!-- LOS DATOS SE CARGARAN PO AJAX -->
			</div>

			<div class="col-md-4 recent">
				<!-- TOPTEN CARGADO POR AJAX -->
			</div>
		</div>
	</div>
</body>
</html>

<script src="lib/js/jquery-3.4.1.min.js"></script>
<script src="lib/js/popper.min.js"></script>
<script src="lib/js/bootstrap.min.js"></script>
<script src="lib/js/sessiones.js"></script>
<script src="lib/js/index.js"></script>