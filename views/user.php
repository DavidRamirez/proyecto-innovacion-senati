<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Perfil</title>

	<link rel="stylesheet" href="lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="lib/icons/css/all.min.css">
	<link rel="stylesheet" href="lib/css/sweetalert2.min.css">
	<link rel="stylesheet" href="lib/css/nav.css">
	<link rel="stylesheet" href="lib/css/user.css">

</head>
<body>
	<nav class="navbar fixed-top navbar-expand-sm">
		<a href="index.php" class="navbar-brand"><img src="image/logos/conversation_32.png">Bienvenido</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="fas fa-bars"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<div class="float-right">
				<button type="button" id="notificacion" class="btn btn-outline-info float-rigth" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom" data-content="
				<div class='float-notificacion'>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div>
				</div>
				">
					<span class="fas fa-bell"></span>
				</button>

				<button class="btn btn-outline-primary dropdown-toggle float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img id="img-user" class="user-nav" <?php echo "data-idusuario='{$_SESSION['id']}' src='image/perfiles_user/".$_SESSION['item_perfil']."'"; ?>>
				</button>
				<div class="dropdown-menu dropdown-menu-lg-right" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="index.php">Inicio</a>
					<a class="dropdown-item" href="user.php">Perfil</a>
					<a class="dropdown-item" href="newQuestions.php">Crear Pregunta</a>
					<div class="dropdown-divider"></div>
					<a id="closeSession" class="dropdown-item" href="#">Cerrar Sesión</a>
				</div>
			</div>
		</div>
	</nav>




	<div class="imagen-banner">
		<img src="image/fondos_user/banner2.jpg">
	</div>

	<div class='container backgroud-banner'>
		<div class='row'>
			<div class='col-sm-12 col-md-5 col-lg-2 img-banner-user'>
			</div>
			<div class='col-sm-12 col-md-7 col-lg-10 img-banner-userdescripcion'>
			</div>
		</div>
	</div>


	<div class='container'>
		<div class='row' id="user-data">
			<!-- INFORMACION DEL USER CARGADA POR AJAX -->
		</div>
	</div>

<div id="test" style="margin-top: 30em;">
	
</div>

	<div class="container">
		<h4>000 puntos <img class="img-nivel" src="image/niveles/pollito.png"> Pollito</h4>
		<h4>050 puntos <img class="img-nivel" src="image/niveles/cachimbo.png"> Cachimbo</h4>
		<h4>125 puntos <img class="img-nivel" src="image/niveles/basico.png"> Basico</h4>
		<h4>200 puntos <img class="img-nivel" src="image/niveles/practicante.png"> Practicante</h4>
		<h4>300 puntos <img class="img-nivel" src="image/niveles/graduado.png"> Graduado</h4>
		<h4>400 puntos <img class="img-nivel" src="image/niveles/m_bronce.png"> Medalla de Bronce</h4>
		<h4>550 puntos <img class="img-nivel" src="image/niveles/m_plata.png"> Medalla de Plata</h4>
		<h4>800 puntos<img class="img-nivel" src="image/niveles/m_oro.png"> Medalla de Oro</h4>
		<h4>1000 puntos<img class="img-nivel" src="image/niveles/m_diamante.png"> Medalla de Diamante</h4>
	</div>

	<br><br>

<!-- MODAL EDITAR USUARIO -->
	<div class="modal fade" id="modalEditUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Edita tus datos</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form id="edit-user">
						<div class="form-group">
							<div class="row">
								<div class="col">
									<input id="nombre" type="text" class="form-control" placeholder="Nombre(s)" maxlength="25">
								</div>

								<div class="col">
									<input id="apellido" type="text" class="form-control" placeholder="Apellidos" maxlength="25">
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col">
									<input id="dni" type="text" class="form-control" placeholder="Dni" maxlength="7">
								</div>

								<div class="col">
									<input id="celular" type="text" class="form-control" placeholder="Celular" maxlength="9">
								</div>
							</div>
						</div>

						<div class="form-group">
							<input id="fechanac" type="date" class="form-control" id="fechaNac" placeholder="Fecha de Nacimiento">
						</div>

						<div class="form-group">
							<input id="email" type="email" class="form-control" placeholder="Correo Electronico" maxlength="50">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal" id="close-modal">Cancelar</button>
					<button id="guardarcambiosuser" type="button" class="btn btn-primary">Guardar cambios</button>
				</div>
			</div>
		</div>
	</div>

<!-- MODAL EDITAR FOTO PERFIL USUARIO -->
	<div class="modal fade" id="modalEditUserItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<form id="form-imagen" enctype="multipart/form-data">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Escoge tu Avatar
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div id="modal-item" class="modal-body">
						<div class='row'>
							<div class='col-sm-3'>
								<a href="#" class='select-item' data-item='user_circle_512.png'>
									<img src='./image/perfiles_user/user_circle_512.png'>
								</a>
							</div>
							<div class='col-sm-3'>
								<a href="#" class='select-item' data-item='user_pro_512.png'>
									<img src='./image/perfiles_user/user_pro_512.png'>
								</a>
							</div>
							<div class='col-sm-3'>
								<a href="#" class='select-item' data-item='man_user_512.png'>
									<img src='./image/perfiles_user/man_user_512.png'>
								</a>
							</div>
							<div class='col-sm-3'>
								<a href="#" class='select-item' data-item='request_512.png'>
									<img src='./image/perfiles_user/request_512.png'>
								</a>
							</div>
						</div>

						<div class='row'>
							<div class='col-sm-3'>
								<a href="#" class='select-item' data-item='reply_512.png'>
									<img src='./image/perfiles_user/reply_512.png'>
									<input type="text" id="testinput" style='display:none;'>
								</a>
							</div>
							<div class='col-sm-3'>
								<a href="#" class='select-item' data-item='user_green_512.png'>
									<img src='./image/perfiles_user/user_green_512.png'>
								</a>
							</div>
							<div class='col-sm-3'>
								<a href="#" class='select-item' data-item='user_512.png'>
									<img src='./image/perfiles_user/user_512.png'>
								</a>
								<input type="text" id="idusuario" name="idusuario" style="display: none;">
							</div>
							<div class='col-sm-3'>
								<div class="custom-file">
									<input type="file" name="itemperfil" class="custom-file-input" id="itemperfil">
									<a href="#" class='select-item' data-item='new_user_512.png' id="img-test">
										<img class="custom-file-label" src='./image/perfiles_user/new_user_512.png'>
									</a>
								</div>
							</div>
						</div>
					</div>

					<div class="modal-footer">
						<button id="cancel-perfil" type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
						<button id="update-perfil" type="submit" class="btn btn-primary" disabled="">Guardar cambios</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>

<script src="lib/js/jquery-3.4.1.min.js"></script>
<script src="lib/js/popper.min.js"></script>
<script src="lib/js/bootstrap.min.js"></script>
<script src="lib/js/sweetalert2.min.js"></script>
<script src="lib/js/sessiones.js"></script>
<script src="lib/js/user.js"></script>
