<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Perfil</title>

	<link rel="stylesheet" href="lib/css/bootstrap.min.css">
	<link rel="stylesheet" href="lib/icons/css/all.min.css">
	<link rel="stylesheet" href="lib/css/sweetalert2.min.css">
	<link rel="stylesheet" href="lib/css/nav.css">
	<link rel="stylesheet" href="lib/css/newQuestions.css">

</head>
<body>
	<nav class="navbar fixed-top navbar-expand-sm">
		<a href="index.php" class="navbar-brand"><img src="image/logos/conversation_32.png">Bienvenido</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="fas fa-bars"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<div class="float-right">
				<button type="button" id="notificacion" class="btn btn-outline-info float-rigth" data-container="body" data-trigger="focus" data-toggle="popover" data-placement="bottom" data-content="
				<div class='float-notificacion'>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div><hr>
					<div><a href='questions.php'>¿Como detener los servicios de windows update?</a>
					</div>
				</div>
				">
					<span class="fas fa-bell"></span>
				</button>

				<button class="btn btn-outline-primary dropdown-toggle float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img id="img-user" class="user-nav" <?php echo "src='image/perfiles_user/".$_SESSION['item_perfil']."'"; ?>>
				</button>
				<div class="dropdown-menu dropdown-menu-lg-right" aria-labelledby="dropdownMenuButton">
					<a class="dropdown-item" href="index.php">Inicio</a>
					<a class="dropdown-item" href="user.php">Perfil</a>
					<a class="dropdown-item" href="#">Crear Pregunta</a>
					<div class="dropdown-divider"></div>
					<a id="closeSession" class="dropdown-item" href="#">Cerrar Sesión</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="margin-top-nav"></div>




	<div class="container" data-idcarrera='<?php echo $_SESSION['idcarrera'] ?>' data-idusuario='<?php echo $_SESSION['id']; ?>'>
		<form id="form-imagen" enctype="multipart/form-data">
			<h1>ESCRIBE TU PREGUNTA</h1>
			<input id="dificultad" type="text" name="dificultad" style="display: none;">
			<input id="numeroinput" type="text" name="numeroinput" style="display: none;">
	<br>
			<input id="question" class="form-control" type="text" placeholder="¿Qué pregunta tienes?" name="question">
	<br>
			<h5>Detalla tu pregunta</h5>
			<textarea id="newquestion" placeholder="Detalla tu pregunta" name="newquestion"></textarea>
	<br>
			<h5>Agrega una URL</h5>
			<input id="link" class="form-control" type="text" placeholder="www.google.com" name="link">
	<br>

			<h5>Agrega archivos para facilitar tu pregunta</h5>
			<div id="crearImput" class="container margin-none">
				<div class="custom-file customFile0">
					<div class="row">
						<div class="col-sm-7">
							<input type="file" name='imagen0' class="custom-file-input" id="customFile0" data-estado='1'>
							<label class="custom-file-label">Máximo 2 megas</label>
						</div>
						<div class="col-sm-1">
							<button id="close0" data-numero='0' data-idInput="customFile0" type="button" class="btn btn-delete-img"><span class="fas fa-times-circle"></span></button>
						</div>
						<div class="col-sm-4" id="img-test">

						</div>
					</div>
				</div>
				
			</div>

			<div id="btn-enviar">
				<button type="button" id="btn-add-inputFile"></button>
				<button id="send-question" type="submit" class="btn btn-primary">Preguntar</button>
			</div>

		</form>
	</div>
	<br><br><br>
	<div id="rpta">
		
	</div>

<br><br><br>
</body>
</html>

<script src="lib/js/jquery-3.4.1.min.js"></script>
<script src="lib/js/ckeditor/ckeditor.js"></script>
<script src="lib/js/popper.min.js"></script>
<script src="lib/js/bootstrap.min.js"></script>
<script src="lib/js/sweetalert2.min.js"></script>
<script src="lib/js/sessiones.js"></script>
<script src="lib/js/newQuestions.js"></script>
